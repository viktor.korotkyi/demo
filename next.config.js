const localeSubpaths = {};

module.exports = {
  env: {
    urlMain: "http://localhost:3000",
    apiUrl: "https://itfgxddwq5.execute-api.us-east-2.amazonaws.com/dev/graphql",
    // apiUrl: "http://localhost:5000/graphql",
    urlPdf: "https://t40i4tqiw8.execute-api.us-east-2.amazonaws.com/dev/pdf",

    tokenMapBox:
      "pk.eyJ1IjoiYXJtb3RlbyIsImEiOiJjazRhN2U4NHgwMTRnM2VteHgwMXV6aGFyIn0.lP2XFXwnZA_A1bGt0C4YFw",
    NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY:
      "pk_test_51IQ7wiEOvBq5OdO01BoEo8eGJalcZ6yYvMeWPB1XpDsmIrW0soDBnaUeNMI3jPE49i2NkxvuO4E0Y9fekJ5Mfan700HW78Qx7I",
    STRIPE_SECRET_KEY:
      "sk_test_51IQ7wiEOvBq5OdO0IFfafcsvoYlxqYeOdQZFHzpI144D8GNGoEUQ0ITA9qF1hllZ5fGRSzTt7crfl6FkwK1oVXup00xc1A3WVt",
  },
  publicRuntimeConfig: {
    localeSubpaths,
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ["@svgr/webpack"],
    });

    return config;
  },
};
