This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

// json-server
json-server --watch -- --port 5000 db.json 


You can start editing the page by modifying `src/pages/index.js`. The page auto-updates as you edit the file.

//local dynamoDB
1. docker run -d -p 8000:8000/tcp amazon/dynamodb-local:latest
2. cd servicesDB
3. npm i
4. npm run dev (create tables and add items in tables)
5. open http://localhost:5000/ with your browser to see the result
6. after 3 second  stop services and you can work
