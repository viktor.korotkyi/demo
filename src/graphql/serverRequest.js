import gql from "graphql-tag";

const request = {
  login: gql`
    mutation login($input: LoginInput!) {
      login(input: $input) {
        auth {
          token
          refreshToken
        }
      }
    }
  `,
  templates: gql`
    query Templates($input: TemplatesInput) {
      Templates(input: $input) {
        templates {
          templateId
          name
          title
          width
          height
          elements
          backgroundImage
        }
      }
    }
  `,
};

export default request;
