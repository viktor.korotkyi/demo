import gql from "graphql-tag";

const localRequest = {
  ID_PORTAL: gql`
    query idPortal {
      idPortal @client
    }
  `,
  ORDER_INFO: gql`
    query orderInfo {
      order @client
    }
  `,
  CHOOSED_ITEMS: gql`
    query orderItems {
      choosedItem @client
    }
  `,
  IS_LOGGED_IN: gql`
    query IsUserLoggedIn {
      token @client
    }
  `,
};

export default localRequest;
