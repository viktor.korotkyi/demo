import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import HeaderHomePage from "../../components/header/HeaderHomePage";

const Header = ({ checkHeader, authStatus }) => {
  const router = useRouter();

  const [openSidebar, setOpenSidebar] = useState(false);
  const [url, setUrl] = useState("");
  const createAnAd = () => {
    router.push("/create-publication");
  };

  const sidebarOpen = () => {
    setOpenSidebar(!openSidebar);
  };

  useEffect(() => {
    if (url !== router.asPath) {
      setUrl(`${router.asPath}`);
      setOpenSidebar(false);
    }
  }, [router.asPath]);

  return (
    <>
      <HeaderHomePage
        authStatus={authStatus}
        sidebarOpen={sidebarOpen}
        createAnAd={createAnAd}
        openSidebar={openSidebar}
        styleMobile={!checkHeader ? "hideHeader" : ""}
      />
    </>
  );
};

Header.defaultProps = {
  checkHeader: true,
  authStatus: false,
  selectedItem: {},
};
Header.propTypes = {
  checkHeader: PropTypes.bool,
  authStatus: PropTypes.bool,
  selectedItem: PropTypes.shape({}),
};

export default Header;
