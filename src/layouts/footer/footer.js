import PropTypes from "prop-types";
import Logo from "../../components/general/Logo";
import Wrapper from "../Wrapper";
import LinkRef from "../../components/general/LinkRef";
import styles from "../../../scss/components/footer/Footer.module.scss";

const Footer = ({ styleFooter }) => (
  <Wrapper styleWrapper={styleFooter} styleContainer={!styleFooter && "containerHide"}>
    <div className={styles.logoBox}>
      <Logo href="/" styleType="logoFooter" urlLogo="/static/images/logoFooter/branding.png" />
    </div>
    <div className={styles.policeBox}>
      <LinkRef href="/privacy" typeStyle="policyLink">
        privacy
      </LinkRef>
      <LinkRef href="/terms-condition" typeStyle="policyLink">
        terms
      </LinkRef>
    </div>
  </Wrapper>
);

Footer.defaultProps = {
  styleFooter: "",
};

Footer.propTypes = {
  styleFooter: PropTypes.string,
};

export default Footer;
