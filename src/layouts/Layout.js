import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import Footer from "./footer/footer";
import Header from "./header/Header";
import styles from "../../scss/layouts/Layout.module.scss";

import Authentication from "../utils/authentication";

const Layout = ({ children }) => {
  const { refresh } = Authentication();
  const [auth, setAuthBool] = useState(true);
  const [selectedItem, setSelectedItem] = useState({
    title: "",
    item: [],
  });

  useEffect(() => {
    refresh();
  }, []);

  const selectItem = (data) => {
    setSelectedItem(data);
  };

  const newChild = React.cloneElement(children, { selectItem });
  return (
    <div className={`${styles.layout}`} id="modal">
      <Header authStatus={auth} selectedItem={selectedItem} />
      <div className={styles.main}>
        <div className={styles.content}>{newChild}</div>
      </div>
      <Footer styleFooter="wrapperFooter" />
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
