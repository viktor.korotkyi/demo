import fetch from "isomorphic-unfetch";

const pdfCreate = async (templateBody) => {
  const defaultBody = {
    backgroundImage: "#ffffff",
    elements: [],
    height: 0,
    name: null,
    templateId: "",
    title: null,
    width: 0,
  };
  const url = process.env.urlPdf;

  Object.keys(defaultBody).forEach((item) => {
    defaultBody[item] = templateBody[item];
  });

  // console.log(JSON.stringify(defaultBody));
  // console.log(defaultBody);
  const res = await fetch(url, {
    method: "POST",
    mode: "cors",
    body: JSON.stringify({ template: defaultBody }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (res.ok) {
    const data = await res.json();
    console.log("kkkkk", data);
  } else {
    console.log(res.status);
  }
};

export default pdfCreate;
