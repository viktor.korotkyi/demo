import { useState, useCallback } from "react";
import { useRouter } from "next/router";
import { useMutation } from "@apollo/client";
import gql from "graphql-tag";
import { initializeApollo } from "../apollo/client";
import { setToLocalStorage, getFromLocalStorage } from "./storageWorks";

const LOGIN_USER = gql`
  mutation login($input: LoginInput!) {
    login(input: $input) {
      auth {
        token
        refreshToken
      }
    }
  }
`;

const IS_LOGGED_IN = gql`
  query IsUserLoggedIn {
    token @client
  }
`;

const Authentication = () => {
  const apolloClient = initializeApollo();
  const router = useRouter();
  const storageName = "tokenUser";
  const storageNameOrder = "orderInfo";
  const [emailUser, setEmailUser] = useState("");
  const [loginUserMutation] = useMutation(LOGIN_USER, {
    update(_, { data }) {
      const tokenUser = { token: data.login.auth.token, email: emailUser };
      setToLocalStorage(storageName, JSON.stringify(tokenUser));
      apolloClient.cache.writeQuery({
        query: IS_LOGGED_IN,
        data: { token: data.login.auth.token },
      });
      router.push("/");
    },
  });

  const loginUser = useCallback((dataUser) => {
    setEmailUser(dataUser.email);
    loginUserMutation({ variables: { input: dataUser } });
  }, []);

  const logout = () => {
    localStorage.removeItem(storageName);
    localStorage.removeItem(storageNameOrder);
    apolloClient.cache.writeQuery({
      query: IS_LOGGED_IN,
      data: { token: "" },
    });
    router.push("/");
  };

  const refresh = () => {
    const newToken = JSON.parse(getFromLocalStorage(storageName));
    if (newToken !== null) {
      apolloClient.cache.writeQuery({
        query: IS_LOGGED_IN,
        data: { token: newToken.token },
      });
    }
  };

  return {
    loginUser,
    logout,
    refresh,
  };
};

export default Authentication;
