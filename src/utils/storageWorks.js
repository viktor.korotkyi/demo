import Router from "next/router";

export const setToLocalStorage = (key, data) => {
  window.localStorage.setItem(key, data);
};

export const getFromLocalStorage = (key) => {
  return window.localStorage.getItem(key);
};

export const getEmptyFormData = (formData) => {
  const emptyData = {};
  Object.keys(formData).forEach((key) => {
    emptyData[key] = "";
  });
  return emptyData;
};

export const getEmptyFormDataBool = (formData) => {
  const emptyData = {};
  Object.keys(formData).forEach((key) => {
    emptyData[key] = false;
  });
  return emptyData;
};

export const formtDate = (date) => {
  const newDate = String(date).split(" ");
  const formteDate = {
    // chooseNewDate: `${newDate[0]}, ${newDate[1]} ${newDate[2]}`,
    chooseNewDate: `${newDate[1]} ${newDate[2]}`,
    chooseYear: newDate[3],
  };
  return formteDate;
};

export const formtDateDetail = (date) => {
  const newDate = String(date).split(" ");
  const formateDate = {
    day: `${newDate[0]}`.padStart(2, "0"),
    date: `${newDate[2]}`,
    month: `${newDate[1]}`,
    year: newDate[3],
  };
  return formateDate;
};

export const daysInMonth = (month, year) => {
  return new Date(year, month, 0).getDate();
};

export const dateActual = () => {
  const today = new Date();
  const newDate = {
    month: today.getMonth(),
    year: today.getFullYear(),
  };
  return newDate;
};

export const createArray = (number) => {
  const newArr = new Array(number).fill("");
  return newArr;
};

export const getWeekDay = (day, year, month) => {
  const days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
  const today = new Date(year, month, day);
  return days[today.getDay()];
};

export const getMonthTitle = (year, month, day = 1) => {
  const monthTitle = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const today = new Date(year, month, day);
  return monthTitle[today.getMonth()];
};

export const checkAvailableDay = (unavailableDays, year, month, day) => {
  const date = new Date(year, month, day);
  const dayNew = `${date.getDate()}`.padStart(2, "0");
  const monthNew = `${date.getMonth() + 1}`.padStart(2, "0");
  const yearNew = `${date.getFullYear()}`;
  const checkDate = `${yearNew}.${monthNew}.${dayNew}`;
  return unavailableDays.indexOf(checkDate) !== -1;
};

export const saveExit = () => {
  Router.push({ pathname: "/" });
};

export const checkObject = (data, data2) => {
  const obj = { ...data, ...data2 };
  const arr =
    Object.values(obj)
      .map((item) => item)
      .indexOf(true) !== -1;
  return arr;
};

export const checkObjectText = (data) =>
  Object.values(data)
    .map((item) => item.text)
    .indexOf("") === -1;

export const checkText = (data) =>
  Object.values(data)
    .map((item) => item.text)
    .filter((el) => el !== "").length > 0;

export const filterItems = (group) => Object.keys(group).filter((item) => group[item] === true);

export const filterArray = (data, item) => data.filter((el) => el.type === item);

export const findIndex = (data, id) => data.findIndex((el) => el.id === id);

export const splitStyleSize = (data) => (data && data.length > 0 ? data.split("px")[0] : "0");
