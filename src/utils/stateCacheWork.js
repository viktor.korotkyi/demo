import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import gql from "graphql-tag";
import { initializeApollo } from "../apollo/client";
import { setToLocalStorage, getFromLocalStorage } from "./storageWorks";

const getOrders = gql`
  query getOrder {
    getOrders {
      orders {
        id
        idOrder
        order {
          id
          name
          cost
        }
      }
    }
  }
`;

const IS_LOGGED_IN = gql`
  query IsUserLoggedIn {
    token @client
  }
`;

const ORDER_INFO = gql`
  query orderInfo {
    order @client
  }
`;

const StateCacheWork = () => {
  const router = useRouter();
  const apolloClient = initializeApollo();
  const token = useQuery(IS_LOGGED_IN);
  const [orderInfo, setOrderInfo] = useState("");
  const storageName = "orderInfo";
  const [viewEdit, setViewEdit] = useState(false);
  const orderPublication = useQuery(getOrders, {
    context: { headers: { Authorization: `Bearer ${token.data ? token.data.token : ""}` } },
  });

  const writeCache = (data) => {
    apolloClient.cache.writeQuery({
      query: ORDER_INFO,
      data: { order: data },
    });
  };

  const getOrderInfo = () => JSON.parse(getFromLocalStorage(storageName));

  useEffect(() => {
    writeCache(orderInfo);
  }, [orderInfo]);

  const edit = () => {
    setViewEdit(!viewEdit);
  };

  const saveExit = () => {
    router.push({ pathname: "/" });
  };

  const handleOrderInfo = (event) => {
    const { value } = event.target;
    setToLocalStorage(storageName, JSON.stringify(value));
    setOrderInfo(value);
  };

  useEffect(() => {
    const resOrder = orderPublication.data && orderPublication.data.getOrders.orders[0].idOrder;
    writeCache(resOrder);
  }, [orderPublication]);

  const refreshData = () => {
    const dataOrder = getOrderInfo();
    if (dataOrder !== null) {
      writeCache(dataOrder);
    }
  };

  return {
    handleOrderInfo,
    edit,
    viewEdit,
    saveExit,
    refreshData,
  };
};

export default StateCacheWork;
