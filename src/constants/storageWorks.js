const listConstatns = {
  portal: "idPortal",
  template: "idTemplate",
  package: "idPackage",
  remember: "remember",
  publication: "publication",
  image: "image",
  textArea: "textArea",
  date: "date",
  marginTop: "marginTop",
  marginRight: "marginRight",
  marginLeft: "marginLeft",
  marginBottom: "marginBottom",
  upload: "upload",
};

export default listConstatns;
