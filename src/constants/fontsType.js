const fontsType = {
  headerOne: "h1",
  headerTwo: "h2",
  headerThree: "h3",
  headerFour: "h4",
  headerFive: "h5",
  headerSix: "h6",
};

export default fontsType;
