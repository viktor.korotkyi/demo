import { useState, useEffect, useCallback } from "react";
import { useQuery, useMutation } from "@apollo/client";
import Wrapper from "../layouts/Wrapper";
import request from "../graphql/serverRequest";
import styles from "../../scss/components/homePage/Home.module.scss";
import Loader from "../components/general/Loader";
import ViewTemplates from "../components/home/ViewTemplates";
import { formtDate, getEmptyFormData, getEmptyFormDataBool } from "../utils/storageWorks";
import { getCroppedImg, getParamsImage } from "../utils/canvasUtils";
import ModalDate from "../components/general/ModalDate";
import EditTemplates from "../components/home/EditTemplates";
import pdfCreate from "../utils/pdfCreate";

const Home = () => {
  const [choosedTemplate, setChoosedTemplate] = useState({});
  const [openEditTemplate, setEditTemplate] = useState(false);
  const [token, setToken] = useState("");
  const [login] = useMutation(request.login, {
    update(_, { data }) {
      setToken(data ? data.login.auth.token : "");
    },
  });

  const [chooseDate, setChooseDate] = useState({
    from: {
      date: "",
      year: "",
    },
    to: {
      date: "",
      year: "",
    },
  });

  const [image, setImage] = useState("");
  const [error, setError] = useState("");
  const [modalTemplate, setModalTemplate] = useState(false);
  const [calendarStatus, setCalendarSatus] = useState({
    from: false,
    to: false,
  });
  const [settingsCrop, setSettingsCrop] = useState({
    crop: { x: 0, y: 0 },
    zoom: 1,
    aspect: 3 / 4,
  });
  const [croppedArea, setCroppedArea] = useState(null);
  const [rotation, setRotation] = useState(0);
  const [addButtonDate, setAddButtonDate] = useState(false);

  useEffect(() => {
    login({
      variables: { input: { email: "prismUser@example.com", password: "12345" } },
    });
  }, []);

  const { data, loading } = useQuery(request.templates, {
    variables: { input: { limit: 100, portalId: "083b3c5e-b4b0-11eb-9234-0123456789ab" } },
    context: { headers: { Authorization: `Bearer ${token}` } },
  });

  const handleEdit = () => {
    setEditTemplate(!openEditTemplate);
  };

  const handleChooseTemplate = (id) => {
    const template = data?.Templates?.templates.filter((el) => el.templateId === id);
    setChoosedTemplate(template[0]);
    handleEdit();
  };

  const getIndexItemDate = () => choosedTemplate.elements.findIndex((el) => el.date === true);
  const saveDate = () => {
    const defaultObj = {
      date: false,
      fixedText: false,
      fontFamily: "",
      fontSize: 0,
      id: "",
      inlineStyleRanges: [],
      margin: [],
      text: "",
      textAlign: "",
      type: "",
      x: 0,
      y: 0,
    };
    const indexItem = getIndexItemDate();
    const itemObj = choosedTemplate !== {} ? choosedTemplate.elements[indexItem] : {};
    Object.keys(defaultObj).forEach((item) => {
      defaultObj[item] = itemObj[item];
    });
    const newArray = [];
    defaultObj.text = `${chooseDate.from.date} ${chooseDate.from.year} - ${chooseDate.to.date} ${chooseDate.to.year}`;
    choosedTemplate.elements.forEach((item) => {
      newArray.push(item);
    });
    newArray[indexItem] = defaultObj;
    setChoosedTemplate({ ...choosedTemplate, elements: newArray });
  };

  useEffect(() => {
    if (choosedTemplate.elements && choosedTemplate?.elements.length > 0) {
      saveDate();
    }
  }, [chooseDate]);

  const getImageUrl = (url, callback) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      const reader = new FileReader();
      reader.onloadend = () => {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open("GET", url);
    xhr.responseType = "blob";
    xhr.send();
  };

  const getIndexItemImage = () => choosedTemplate.elements.findIndex((el) => el.type === "image");
  const saveDateImage = () => {
    const defaultObj = {
      alignment: "",
      height: 0,
      id: "",
      margin: [],
      type: "",
      url: "",
      width: 0,
      x: 0,
      y: 0,
      z: true,
    };
    const indexItem = getIndexItemImage();
    const itemObj = choosedTemplate !== {} ? choosedTemplate.elements[indexItem] : {};
    Object.keys(defaultObj).forEach((item) => {
      defaultObj[item] = itemObj[item];
    });
    const newArray = [];

    defaultObj.url = image;
    choosedTemplate.elements.forEach((item) => {
      newArray.push(item);
    });
    newArray[indexItem] = defaultObj;
    setChoosedTemplate({ ...choosedTemplate, elements: newArray });
  };

  useEffect(() => {
    if (choosedTemplate.elements && choosedTemplate?.elements.length > 0) {
      saveDateImage();
    }
  }, [image]);

  const openCalendar = (id) => {
    const status = getEmptyFormDataBool(calendarStatus);
    setCalendarSatus({ ...status, [id]: !calendarStatus[id] });
  };

  const handleChooseDateFrom = (event) => {
    setChooseDate({
      ...chooseDate,
      from: { date: formtDate(event).chooseNewDate, year: formtDate(event).chooseYear },
    });
    openCalendar();
  };

  const handleChooseDateTo = (event) => {
    setChooseDate({
      ...chooseDate,
      to: { date: formtDate(event).chooseNewDate, year: formtDate(event).chooseYear },
    });
    openCalendar();
  };

  const handleYearDate = (event) => {
    const { id, value } = event.target;
    setChooseDate({ ...chooseDate, [id]: { date: chooseDate[id].date, year: value } });
  };

  const addDate = () => {
    setChooseDate({ ...chooseDate, to: getEmptyFormData(chooseDate.to) });
    setAddButtonDate(!addButtonDate);
  };

  const openModalDate = () => {
    setModalTemplate(!modalTemplate);
  };

  const getIndexItem = (id) => choosedTemplate.elements.findIndex((el) => el.id === id);

  const handleTextPub = (event) => {
    const { id, value } = event.target;
    const defaultObj = {
      date: false,
      fixedText: false,
      fontFamily: "",
      fontSize: 0,
      id: "",
      inlineStyleRanges: [],
      margin: [],
      text: "",
      textAlign: "",
      type: "",
      x: 0,
      y: 0,
    };
    const newArray = [];
    const indexItem = getIndexItem(id);
    const itemObj = choosedTemplate.elements[indexItem];
    Object.keys(defaultObj).forEach((item) => {
      defaultObj[item] = itemObj[item];
    });
    defaultObj.text = value;
    choosedTemplate.elements.forEach((item) => {
      newArray.push(item);
    });
    newArray[indexItem] = defaultObj;
    setChoosedTemplate({ ...choosedTemplate, elements: newArray });
  };

  const onCropChange = (crop) => {
    setSettingsCrop({ ...settingsCrop, crop });
  };

  const onCropComplete = useCallback((_, croppedAreaPixels) => {
    setCroppedArea(croppedAreaPixels);
  }, []);

  const onZoomChange = (zoom) => {
    setSettingsCrop({ ...settingsCrop, zoom });
  };

  const handleCroppedImage = useCallback(async () => {
    try {
      const croppedImage = await getCroppedImg(image, croppedArea, rotation);
      getImageUrl(croppedImage, (dataUrl) => {
        setImage(dataUrl);
      });
    } catch (e) {
      setError(e);
    }
  }, [image, croppedArea, rotation]);

  const handleRotationImage = useCallback(
    async (directRotation) => {
      try {
        const rotate = directRotation === "left" ? rotation - 90 : rotation + 90;
        const params = await getParamsImage(image);
        const croppedImage = await getCroppedImg(image, params, rotate);
        getImageUrl(croppedImage, (dataUrl) => {
          setImage(dataUrl);
        });
      } catch (e) {
        setError(e);
      }
    },
    [image, rotation]
  );

  const rotateLeft = async () => {
    handleRotationImage("left");
  };

  const rotateRight = async () => {
    handleRotationImage("right");
  };

  const uploadFile = (event) => {
    if (event.target.files && event.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        getImageUrl(reader.result, (dataUrl) => {
          setImage(dataUrl);
        });
      });
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const deleteUploadFile = () => {
    setImage("");
  };

  const createPdf = () => {
    pdfCreate(choosedTemplate);
  };

  return (
    <div className={styles.container}>
      {!loading ? (
        <Wrapper styleWrapper="wrapperHome">
          {!openEditTemplate ? (
            <ViewTemplates
              handleChooseTemplate={handleChooseTemplate}
              templateList={data?.Templates?.templates}
            />
          ) : (
            <EditTemplates
              choosedTemplate={choosedTemplate && choosedTemplate}
              back={handleEdit}
              textFirst="from"
              textSecond="to"
              dateFirst={`${
                chooseDate.from.date ? `${chooseDate.from.date} ${chooseDate.from.year}` : ""
              }`}
              dateSecond={`${
                chooseDate.to.date ? `${chooseDate.to.date} ${chooseDate.to.year}` : ""
              }`}
              openModalDate={openModalDate}
              imageSrc={image}
              uploadFile={uploadFile}
              deleteUploadFile={deleteUploadFile}
              rotateLeft={rotateLeft}
              rotateRight={rotateRight}
              onRotationChange={setRotation}
              rotation={rotation}
              handleCroppedImage={handleCroppedImage}
              settingsCrop={settingsCrop}
              onCropChange={onCropChange}
              onCropComplete={onCropComplete}
              onZoomChange={onZoomChange}
              handleTextPub={handleTextPub}
              chooseDate={chooseDate}
              createPdf={createPdf}
            />
          )}
          {modalTemplate && (
            <ModalDate
              closeModal={openModalDate}
              headerFromDate="from"
              headerToDate="to"
              handleYearDate={handleYearDate}
              openCalendar={openCalendar}
              calendarStatus={calendarStatus}
              handleChooseDateFrom={handleChooseDateFrom}
              handleChooseDateTo={handleChooseDateTo}
              chooseDate={chooseDate}
              addButtonDate={addButtonDate}
              addDate={addDate}
              textBtdd="addDate"
              remove="remove"
            />
          )}
        </Wrapper>
      ) : (
        <Loader />
      )}
    </div>
  );
};

export default Home;
