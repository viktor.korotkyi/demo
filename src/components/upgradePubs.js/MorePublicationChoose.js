import PropTypes from "prop-types";
import ListGroupNewspapers from "../general/ListGroupNewspapers";
import CardNewspapersMini from "../general/CardNewspaperMini";
import HeaderUpgradeView from "../general/HeaderUpgradeView";
import styles from "../../../scss/components/upgradePubs/MorePublicationChoose.module.scss";

const MorePublicationChoose = ({
  title,
  underTitle,
  text,
  header,
  choosedItem,
  listChoose,
  removePub,
  addPub,
  choosed,
}) => (
  <div className={styles.container}>
    <div className={styles.boxChoosedPub}>
      <HeaderUpgradeView title={title} underTitle={underTitle} text={text} />
      <h3 className={styles.header}>{header}</h3>
      <div className={styles.listChoosed}>
        <div className={styles.scroll}>
          {choosedItem &&
            choosedItem.length > 0 &&
            choosedItem.map((item, index) => (
              <CardNewspapersMini
                key={[index, item.name].join("_")}
                header={item.name}
                img={item.logo}
                info={item.description}
                handleActive={removePub}
                checkCard={choosed[item.name]}
              />
            ))}
        </div>
      </div>
    </div>
    <div className={styles.listChooseItem}>
      <div className={styles.scroll}>
        <ListGroupNewspapers list={listChoose} handleGroup={addPub} hideBtn />
      </div>
    </div>
  </div>
);

MorePublicationChoose.defaultProps = {
  choosedItem: [],
  listChoose: [],
  title: "",
  underTitle: "",
  text: "",
  header: "",
  choosed: {},
  removePub: () => {},
  addPub: () => {},
};

MorePublicationChoose.propTypes = {
  choosedItem: PropTypes.arrayOf(PropTypes.shape({})),
  listChoose: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  underTitle: PropTypes.string,
  text: PropTypes.string,
  header: PropTypes.string,
  removePub: PropTypes.func,
  addPub: PropTypes.func,
  choosed: PropTypes.shape({}),
};

export default MorePublicationChoose;
