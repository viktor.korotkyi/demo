import PropTypes from "prop-types";
import Logo from "../general/Logo";
import Button from "../UI/Button";
import styles from "../../../scss/components/reviewOrder/HeaderReviewOrder.module.scss";
import LogoFooter from "../general/LogoFooter";

const HeaderReviewOrder = ({ text, saveExit, btnText, privacy, terms }) => (
  <div className={styles.container}>
    <Logo href="/" urlLogo="/static/images/logo.png" styleType="logo" />
    <div className={styles.save}>
      <span>{text}</span>
      <Button color="buttonTransparentColor" onClick={saveExit}>
        {btnText}
      </Button>
    </div>
    <LogoFooter
      href="/"
      logoStyle="logoFooter"
      logo="logoMain"
      privacy={privacy}
      terms={terms}
      urlLogo="/static/images/logoFooter/branding.png"
    />
  </div>
);

HeaderReviewOrder.defaultProps = {
  text: "",
  btnText: "",
  privacy: "",
  terms: "",
  saveExit: () => {},
};

HeaderReviewOrder.propTypes = {
  text: PropTypes.string,
  btnText: PropTypes.string,
  privacy: PropTypes.string,
  terms: PropTypes.string,
  saveExit: PropTypes.func,
};

export default HeaderReviewOrder;
