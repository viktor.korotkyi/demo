import PropTypes from "prop-types";
import HeaderBox from "../general/HeaderBox";
import NewspaperBar from "../general/NewspaperBar";
import PrintSocialView from "../general/PrintSocialView";
import TextBtnOrderDetails from "../general/TextBtnOrderDetails";
import styles from "../../../scss/components/reviewOrder/OrderDetailReview.module.scss";

const OrderDetailReview = ({
  headerOrderDetail,
  textOrderDetail,
  edit,
  open,
  editPubDate,
  openStatus,
  listChoosed,
  yourPublicationDates,
  editPrint,
  headerPrint,
  srcImg,
  editSocial,
  headerSocial,
  srcImgSocial,
  editPub,
  yourChosenPub,
}) => (
  <div className={styles.container}>
    <HeaderBox
      header={headerOrderDetail}
      styleHeader="headerLeftPositionCap"
      text={textOrderDetail}
    />
    <TextBtnOrderDetails btnText={edit} text={yourChosenPub} onClick={editPub} />
    {listChoosed.map((item, index) => (
      <NewspaperBar
        key={[item.name, index].join("_")}
        id={item.name}
        btnText={edit}
        img={item.logo}
        newspaper={item.name}
        open={open}
        editPubDate={editPubDate}
        yourPublicationDates={yourPublicationDates}
        openStatus={openStatus[item.name]}
        newspaperUnder={item.description}
        date={item.date}
      />
    ))}
    <div className={styles.viewPrint}>
      <PrintSocialView
        edit={edit}
        editPrint={editPrint}
        headerPrint={headerPrint}
        srcImg={srcImg}
      />
      <PrintSocialView
        edit={edit}
        editPrint={editSocial}
        headerPrint={headerSocial}
        srcImg={srcImgSocial}
      />
    </div>
  </div>
);

OrderDetailReview.defaultProps = {
  headerOrderDetail: "",
  textOrderDetail: "",
  edit: "",
  yourPublicationDates: "",
  headerPrint: "",
  srcImg: "",
  headerSocial: "",
  srcImgSocial: "",
  yourChosenPub: "",
  editPub: () => {},
  open: () => {},
  editPubDate: () => {},
  editSocial: () => {},
  editPrint: () => {},
  listChoosed: [],
  openStatus: {},
};

OrderDetailReview.propTypes = {
  textOrderDetail: PropTypes.string,
  headerOrderDetail: PropTypes.string,
  edit: PropTypes.string,
  yourPublicationDates: PropTypes.string,
  headerPrint: PropTypes.string,
  srcImg: PropTypes.string,
  headerSocial: PropTypes.string,
  srcImgSocial: PropTypes.string,
  yourChosenPub: PropTypes.string,
  editPub: PropTypes.func,
  open: PropTypes.func,
  editPubDate: PropTypes.func,
  editSocial: PropTypes.func,
  openStatus: PropTypes.shape({}),
  editPrint: PropTypes.func,
  listChoosed: PropTypes.arrayOf(PropTypes.shape({})),
};

export default OrderDetailReview;
