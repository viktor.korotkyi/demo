import PropTypes from "prop-types";
import Wrapper from "../../layouts/Wrapper";
import HeaderBox from "../general/HeaderBox";
import CardTemplate from "../general/CardTemplate";
import constants from "../../constants/staticFile";
import styles from "../../../scss/components/chooseTemplate/TemplateList.module.scss";

const TemplateList = ({
  header,
  text,
  templateList,
  headerContent,
  selectOneText,
  chooseTemplate,
  openModal,
}) => (
  <div className={styles.container}>
    <Wrapper styleWrapper="wrapperCreatePublication" styleContainer="containerHightAuto">
      <HeaderBox header={header} text={text} />
      <div className={styles.content}>
        <h3>{headerContent}</h3>
        <div className={styles.boxTemplates}>
          {templateList &&
            templateList.map((item, index) => (
              <CardTemplate
                id={item.templateId}
                key={[index, item.templateId].join("_")}
                selectOneText={selectOneText}
                src={item.logo ? item.logo : constants.template}
                chooseTemplate={chooseTemplate}
                openModal={openModal}
              />
            ))}
        </div>
      </div>
    </Wrapper>
  </div>
);

TemplateList.defaultProps = {
  header: "",
  text: "",
  headerContent: "",
  selectOneText: "",
  templateList: [],
  chooseTemplate: () => {},
  openModal: () => {},
};

TemplateList.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  headerContent: PropTypes.string,
  selectOneText: PropTypes.string,
  templateList: PropTypes.arrayOf(PropTypes.shape({})),
  chooseTemplate: PropTypes.func,
  openModal: PropTypes.func,
};

export default TemplateList;
