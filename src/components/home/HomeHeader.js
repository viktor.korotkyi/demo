import PropTypes from "prop-types";

import styles from "../../../scss/components/homePage/HomeHeader.module.scss";

const HomeHeader = ({ header, text }) => (
  <div className={styles.container}>
    <h3>{header}</h3>
    <span>{text}</span>
  </div>
);

HomeHeader.defaultProps = {
  header: "",
  text: "",
};

HomeHeader.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
};

export default HomeHeader;
