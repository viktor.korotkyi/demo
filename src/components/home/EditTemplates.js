import PropTypes from "prop-types";
import styles from "../../../scss/components/homePage/EditTemplates.module.scss";
import FormBuildPub from "../general/FormBuildPub";
import LivePreview from "../general/LivePreview";
import Button from "../UI/Button";

const EditTemplates = ({
  back,
  choosedTemplate,
  headerDate,
  textFirst,
  textSecond,
  dateFirst,
  dateSecond,
  openModalDate,
  imageSrc,
  uploadFile,
  deleteUploadFile,
  rotateLeft,
  rotateRight,
  onRotationChange,
  rotation,
  handleCroppedImage,
  settingsCrop,
  onCropChange,
  onCropComplete,
  onZoomChange,
  handleTextPub,
  chooseDate,
  createPdf,
}) => (
  <div className={styles.container}>
    <div className={styles.formBox}>
      <div className={styles.boxBtn}>
        <Button color="buttonBack" onClick={back}>
          {`< Back`}
        </Button>
        <Button color="buttonBack" onClick={createPdf}>
          Save
        </Button>
      </div>

      <FormBuildPub
        choosedTemplate={choosedTemplate}
        header={headerDate}
        textFirst={textFirst}
        textSecond={textSecond}
        dateFirst={dateFirst}
        dateSecond={dateSecond}
        openModalDate={openModalDate}
        imageSrc={imageSrc}
        uploadFile={uploadFile}
        deleteUploadFile={deleteUploadFile}
        rotateLeft={rotateLeft}
        rotateRight={rotateRight}
        onRotationChange={onRotationChange}
        rotation={rotation}
        handleCroppedImage={handleCroppedImage}
        settingsCrop={settingsCrop}
        onCropChange={onCropChange}
        onCropComplete={onCropComplete}
        onZoomChange={onZoomChange}
        handleTextPub={handleTextPub}
      />
    </div>

    <div className={styles.viewContainer}>
      <LivePreview choosedTemplate={choosedTemplate} chooseDate={chooseDate} image={imageSrc} />
    </div>
  </div>
);

EditTemplates.defaultProps = {
  back: () => {},
  uploadFile: () => {},
  deleteUploadFile: () => {},
  rotateLeft: () => {},
  rotateRight: () => {},
  chooseDate: {},
  createPdf: () => {},
  choosedTemplate: {},
  headerDate: "",
  textFirst: "",
  textSecond: "",
  dateFirst: "",
  dateSecond: "",
  imageSrc: "",
  openModalDate: () => {},
  onRotationChange: () => {},
  rotation: 0,
  handleCroppedImage: () => {},
  settingsCrop: {},
  onCropChange: () => {},
  onCropComplete: () => {},
  onZoomChange: () => {},
  handleTextPub: () => {},
};

EditTemplates.propTypes = {
  back: PropTypes.func,
  choosedTemplate: PropTypes.shape({}),
  headerDate: PropTypes.string,
  textFirst: PropTypes.string,
  textSecond: PropTypes.string,
  dateFirst: PropTypes.string,
  dateSecond: PropTypes.string,
  openModalDate: PropTypes.func,
  imageSrc: PropTypes.string,
  uploadFile: PropTypes.func,
  deleteUploadFile: PropTypes.func,
  rotateLeft: PropTypes.func,
  rotateRight: PropTypes.func,
  onRotationChange: PropTypes.func,
  rotation: PropTypes.number,
  handleCroppedImage: PropTypes.func,
  settingsCrop: PropTypes.shape({}),
  onCropChange: PropTypes.func,
  onCropComplete: PropTypes.func,
  onZoomChange: PropTypes.func,
  handleTextPub: PropTypes.func,
  chooseDate: PropTypes.shape({}),
  createPdf: PropTypes.func,
};

export default EditTemplates;
