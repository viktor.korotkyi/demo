import PropTypes from "prop-types";
import ItemListCity from "../general/ItemListCity";
import styles from "../../../scss/components/homePage/HomeRegion.module.scss";

const HomeRegion = ({ header, listCity }) => (
  <div className={styles.container}>
    <h3 className={styles.headerRegion}>{header}</h3>
    {listCity &&
      Object.values(listCity).map((item, index) => (
        <ItemListCity key={[index, item.name].join("_")} city={item.name} number={`${index + 1}`} />
      ))}
  </div>
);

HomeRegion.defaultProps = {
  header: "",
  listCity: [],
};

HomeRegion.propTypes = {
  header: PropTypes.string,
  listCity: PropTypes.shape({}),
};

export default HomeRegion;
