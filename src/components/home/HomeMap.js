import PropTypes from "prop-types";
import Map from "../general/Map";
import HomeRegion from "./HomeRegion";
import styles from "../../../scss/components/homePage/HomeMap.module.scss";

const HomeMap = ({ dataMap, dataMarker, header, nameRegion }) => (
  <div className={styles.container}>
    <h3 className={styles.mapHeader}>{header}</h3>
    <div className={styles.mapContainer}>
      <Map dataMap={dataMap} dataMarker={dataMarker} />
      <div className={styles.regionBox}>
        {dataMarker.map((item, index) => (
          <HomeRegion key={[item, index].join("_")} listCity={item} header={nameRegion} />
        ))}
      </div>
    </div>
  </div>
);

HomeMap.defaultProps = {
  dataMap: {},
  dataMarker: [],
  header: "",
  nameRegion: "",
};

HomeMap.propTypes = {
  dataMap: PropTypes.shape({}),
  dataMarker: PropTypes.arrayOf(PropTypes.shape({})),
  header: PropTypes.string,
  nameRegion: PropTypes.string,
};

export default HomeMap;
