import PropTypes from "prop-types";
import TemplateList from "../chooseTemplate/TemplateList";
import styles from "../../../scss/components/homePage/ViewTemplates.module.scss";

const ViewTemplates = ({ templateList, handleChooseTemplate }) => (
  <div className={styles.container}>
    <TemplateList
      header="Available Templates"
      text="Choose One Template"
      templateList={templateList}
      selectOneText="Select This One"
      headerContent="Category"
      activeTemplate={templateList}
      chooseTemplate={handleChooseTemplate}
    />
  </div>
);

ViewTemplates.defaultProps = {
  templateList: [],
  handleChooseTemplate: () => {},
};

ViewTemplates.propTypes = {
  templateList: PropTypes.arrayOf(PropTypes.shape({})),
  handleChooseTemplate: PropTypes.func,
};

export default ViewTemplates;
