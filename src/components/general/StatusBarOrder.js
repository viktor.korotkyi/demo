import PropTypes from "prop-types";
import StatusIcon from "./StatusIcon";
import styles from "../../../scss/components/general/StatusBarOrder.module.scss";

const StatusBarOrder = ({ firstStep, secondStep, lastStep, statusOrder }) => (
  <div className={styles.container}>
    <div className={styles.statusBar}>
      <StatusIcon status={statusOrder.firstStep} />
      <StatusIcon status={statusOrder.secondStep} visible={statusOrder.firstStep} />
      <StatusIcon status={statusOrder.lastStep} visible={statusOrder.secondStep} />
    </div>
    <div className={styles.statusText}>
      <span className={statusOrder.firstStep !== true && styles.activity}>{firstStep}</span>
      <span className={statusOrder.secondStep !== true && styles.activity}>{secondStep}</span>
      <span className={statusOrder.lastStep !== true && styles.activity}>{lastStep}</span>
    </div>
  </div>
);

StatusBarOrder.defaultProps = {
  firstStep: "",
  secondStep: "",
  lastStep: "",
  statusOrder: {},
};

StatusBarOrder.propTypes = {
  firstStep: PropTypes.string,
  secondStep: PropTypes.string,
  lastStep: PropTypes.string,
  statusOrder: PropTypes.shape({
    firstStep: PropTypes.bool,
    secondStep: PropTypes.bool,
    lastStep: PropTypes.bool,
  }),
};

export default StatusBarOrder;
