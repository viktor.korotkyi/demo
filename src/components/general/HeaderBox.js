import PropTypes from "prop-types";
import styles from "../../../scss/components/general/HeaderBox.module.scss";

const HeaderBox = ({ header, text, styleHeader }) => (
  <div className={`${styles.header} ${styleHeader && styles[styleHeader]}`}>
    <h3>{header}</h3>
    <span>{text}</span>
  </div>
);

HeaderBox.defaultProps = {
  header: "",
  text: "",
  styleHeader: "",
};

HeaderBox.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  styleHeader: PropTypes.string,
};

export default HeaderBox;
