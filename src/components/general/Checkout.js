import PropTypes from "prop-types";
import { useQuery, useMutation } from "@apollo/client";
import StripeCheckout from "react-stripe-checkout";
import gql from "graphql-tag";
import styles from "../../../scss/components/general/Checkout.module.scss";
import HeaderBox from "./HeaderBox";
import OrderNumber from "./OrderNumber";
import PaymentOptions from "./PaymentOptions";
import PricingBoxOrder from "./PricingBoxOrder";

const createSubscription = gql`
  mutation createSubscription($source: String!) {
    createSubscription(source: $source) {
      email
      idStripe
    }
  }
`;

const IS_LOGGED_IN = gql`
  query IsUserLoggedIn {
    token @client
  }
`;

const Checkout = ({
  header,
  text,
  btnText,
  edit,
  headerOrder,
  textOrder,
  order,
  priceList,
  total,
  headerOption,
  firstBtn,
  secondBtn,
  firstBtnClick,
  secondBtnClick,
  orderInfo,
  viewEdit,
  onChange,
  packageItem,
  facebook,
  nameTypographic,
  description,
  label,
  panelLabel,
}) => {
  const tokenUser = useQuery(IS_LOGGED_IN);
  const [subscribe] = useMutation(createSubscription, {
    context: { headers: { authorization: tokenUser.data ? `Bearer ${tokenUser.data.token}` : "" } },
  });

  const onToken = async (token) => {
    const payItem = await token;
    subscribe({ variables: { source: payItem.id } });
  };
  return (
    <div className={styles.container}>
      <HeaderBox header={header} styleHeader="headerLeftPosition" text={text} />
      <OrderNumber
        btnText={btnText}
        edit={edit}
        header={headerOrder}
        text={textOrder}
        order={order}
        viewEdit={viewEdit}
        orderInfo={orderInfo}
        onChange={onChange}
      />
      <PricingBoxOrder
        priceList={priceList}
        total={total}
        stylePrice="payNow"
        packageItem={packageItem}
        facebook={facebook}
        hideBtn
      />
      <StripeCheckout
        token={onToken}
        className={styles.payNowButton}
        image="/static/images/Logo-typographic.png"
        name={nameTypographic}
        description={description}
        label={label}
        panelLabel={panelLabel}
        stripeKey={process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY}
      />
      <PaymentOptions
        firstBtn={firstBtn}
        firstBtnClick={firstBtnClick}
        header={headerOption}
        secondBtn={secondBtn}
        secondBtnClick={secondBtnClick}
      />
    </div>
  );
};
Checkout.defaultProps = {
  header: "",
  text: "",
  btnText: "",
  headerOrder: "",
  textOrder: "",
  order: "",
  total: "",
  orderInfo: "",
  headerOption: "",
  firstBtn: "",
  nameTypographic: "",
  description: "",
  label: "",
  panelLabel: "",
  viewEdit: false,
  priceList: [],
  packageItem: {},
  facebook: {},
  secondBtn: "",
  edit: () => {},
  firstBtnClick: () => {},
  secondBtnClick: () => {},
  onChange: () => {},
};

Checkout.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  btnText: PropTypes.string,
  headerOrder: PropTypes.string,
  textOrder: PropTypes.string,
  order: PropTypes.string,
  total: PropTypes.string,
  headerOption: PropTypes.string,
  firstBtn: PropTypes.string,
  orderInfo: PropTypes.string,
  secondBtn: PropTypes.string,
  nameTypographic: PropTypes.string,
  description: PropTypes.string,
  label: PropTypes.string,
  panelLabel: PropTypes.string,
  viewEdit: PropTypes.bool,
  firstBtnClick: PropTypes.func,
  secondBtnClick: PropTypes.func,
  edit: PropTypes.func,
  onChange: PropTypes.func,
  packageItem: PropTypes.shape({}),
  facebook: PropTypes.shape({}),
  priceList: PropTypes.arrayOf(PropTypes.shape({})),
};

export default Checkout;
