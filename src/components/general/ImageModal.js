import PropTypes from "prop-types";
import ModalLayout from "../../layouts/ModalLayout";
import CloseButton from "./CloseButton";
import styles from "../../../scss/components/general/ImageModal.module.scss";

const ImageModal = ({ src, closeModal }) => (
  <ModalLayout typeContainer="template">
    <div className={styles.container}>
      <div className={styles.btnBox}>
        <CloseButton click={closeModal} closeType="closeTemplate" />
      </div>
      <div className={styles.content}>
        <img src={src} alt="template" />
      </div>
    </div>
  </ModalLayout>
);

ImageModal.defaultProps = {
  src: "",
  closeModal: () => {},
};

ImageModal.propTypes = {
  src: PropTypes.string,
  closeModal: PropTypes.func,
};

export default ImageModal;
