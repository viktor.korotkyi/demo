import PropTypes from "prop-types";
import styles from "../../../scss/components/general/HeaderActiveModal.module.scss";

const HeaderActiveModal = ({ stylesType, statusModal, enterZipCode, count, totalCount }) => (
  <div className={`${styles[stylesType]} ${!statusModal && styles.active}`}>
    <span>{`${count} of ${totalCount}`}</span>
    <span>{enterZipCode}</span>
  </div>
);

HeaderActiveModal.defaultProps = {
  stylesType: "",
  statusModal: false,
  enterZipCode: "",
  count: "",
  totalCount: "",
};

HeaderActiveModal.propTypes = {
  stylesType: PropTypes.string,
  statusModal: PropTypes.bool,
  enterZipCode: PropTypes.string,
  count: PropTypes.string,
  totalCount: PropTypes.string,
};

export default HeaderActiveModal;
