import PropTypes from "prop-types";
import styles from "../../../scss/components/general/CloseButton.module.scss";

const CloseButton = ({ click, closeType }) => (
  <button type="button" onClick={click} className={styles.btnClose}>
    <span className={styles[closeType]} />
  </button>
);
CloseButton.defaultProps = {
  closeType: "close",
  click: () => {},
};

CloseButton.propTypes = {
  click: PropTypes.func,
  closeType: PropTypes.string,
};

export default CloseButton;
