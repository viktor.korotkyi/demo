import PropTypes from "prop-types";
import { Marker } from "react-map-gl";

import styles from "../../../scss/components/general/MarkerMap.module.scss";

const MarkerMap = ({ dataMarker }) =>
  dataMarker &&
  Object.values(dataMarker).map((item, index) => (
    <Marker
      key={[index, item.name].join("_")}
      latitude={item.latitude}
      longitude={item.longitude}
      offsetLeft={item.offsetLeft}
      offsetTop={item.offsetTop}
    >
      <div className={styles.MarkerBtn}>
        <span>{`${index + 1}`}</span>
      </div>
    </Marker>
  ));

MarkerMap.defaultProps = {
  dataMarker: {},
};

MarkerMap.propTypes = {
  dataMarker: PropTypes.shape({}),
};

export default MarkerMap;
