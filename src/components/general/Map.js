import React, { useState } from "react";
import PropTypes from "prop-types";
import ReactMapGl from "react-map-gl";
import MarkerMap from "./MarkerMap";
import styles from "../../../scss/components/general/Map.module.scss";

const Map = ({ dataMap, dataMarker }) => {
  const [viewport, setViewport] = useState({
    latitude: dataMap.latitude,
    longitude: dataMap.longitude,
    width: dataMap.width,
    height: dataMap.height,
    zoom: dataMap.zoom,
  });

  return (
    <div className={styles.MyMap}>
      <ReactMapGl
        {...viewport}
        mapboxApiAccessToken={process.env.tokenMapBox}
        mapStyle="mapbox://styles/armoteo/ck4b2mnej0z301cpcr47k5962"
        onViewportChange={() => {
          setViewport({ ...viewport });
        }}
      >
        {dataMarker &&
          dataMarker.map((item, index) => (
            <MarkerMap key={[index, item].join("_")} dataMarker={item} />
          ))}
      </ReactMapGl>
    </div>
  );
};

Map.defaultProps = {
  dataMap: {},
  dataMarker: [],
};

Map.propTypes = {
  dataMap: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    width: PropTypes.string,
    height: PropTypes.number,
    zoom: PropTypes.number,
  }),
  dataMarker: PropTypes.arrayOf(PropTypes.shape({})),
};

export default Map;
