import PropTypes from "prop-types";
import styles from "../../../scss/components/general/HeaderUpgradeView.module.scss";

const HeaderUpgradeView = ({ title, underTitle, text }) => (
  <div className={styles.container}>
    <div className={styles.titleBox}>
      <span>{title}</span>
      <span>{underTitle}</span>
    </div>
    <span className={styles.text}>{text}</span>
  </div>
);

HeaderUpgradeView.defaultProps = {
  title: "",
  underTitle: "",
  text: "",
};

HeaderUpgradeView.propTypes = {
  title: PropTypes.string,
  underTitle: PropTypes.string,
  text: PropTypes.string,
};

export default HeaderUpgradeView;
