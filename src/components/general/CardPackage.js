import PropTypes from "prop-types";
import styles from "../../../scss/components/general/CardPackage.module.scss";

const CardPackage = ({ id, price, name, info, clickCard, active }) => (
  <div className={`${styles.container} ${active && styles.active}`} onClick={() => clickCard(id)}>
    <h3>{`$${price}`}</h3>
    <h3>{name}</h3>
    <span>{info}</span>
  </div>
);

CardPackage.defaultProps = {
  id: "",
  price: 0,
  name: "",
  info: "",
  clickCard: () => {},
  active: false,
};

CardPackage.propTypes = {
  id: PropTypes.string,
  price: PropTypes.number,
  name: PropTypes.string,
  info: PropTypes.string,
  clickCard: PropTypes.func,
  active: PropTypes.bool,
};

export default CardPackage;
