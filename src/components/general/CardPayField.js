import PropTypes from "prop-types";
import { faCreditCard, faUniversity } from "@fortawesome/free-solid-svg-icons";
import InputBox from "../UI/InputBox";
import Button from "../UI/Button";
import IconView from "../UI/IconView";
import styles from "../../../scss/components/general/CardPayField.module.scss";
import IconPay from "../UI/IconPay";

const CardPayFieald = ({
  total,
  infoPay,
  usdDueOn,
  date,
  card,
  bank,
  activePayType,
  handleActivePayType,
  onChange,
  value,
  handleActiveInput,
  activeInput,
  cardNumber,
  mmYY,
  cvc,
  checkInput,
  payInvoice,
}) => (
  <div className={styles.container}>
    <h3>{`$${total} ${usdDueOn} ${date}`}</h3>
    <span className={styles.info}>{infoPay}</span>
    <div className={styles.typePayContainer}>
      <IconPay
        id="faCreditCard"
        card={card}
        faIcon={faCreditCard}
        handleActivePayType={handleActivePayType}
        activePayType={activePayType}
      />
      <IconPay
        id="faUniversity"
        card={bank}
        faIcon={faUniversity}
        handleActivePayType={handleActivePayType}
        activePayType={activePayType}
      />
    </div>
    {!activeInput ? (
      <Button onClick={handleActiveInput} color="payCardActive">
        <div className={styles.card}>
          <div className={styles.cardText}>
            <IconView icon={faCreditCard} styleType="pay" />
            <span>{cardNumber}</span>
          </div>
          <div className={styles.infoPayCard}>
            <span>{mmYY}</span>
            <span>{cvc}</span>
          </div>
        </div>
      </Button>
    ) : (
      <InputBox
        autocomplete="off"
        onChange={onChange}
        styleType="cardNumber"
        value={value}
        onBlur={checkInput}
        autofocus
      />
    )}
    <div className={styles.buttonContainer}>
      <Button color="blueSubmit" typeBtn={false}>
        {payInvoice}
      </Button>
    </div>
  </div>
);

CardPayFieald.defaultProps = {
  total: "",
  infoPay: "",
  usdDueOn: "",
  date: "",
  card: "",
  bank: "",
  value: "",
  cardNumber: "",
  mmYY: "",
  cvc: "",
  payInvoice: "",
  activePayType: false,
  activeInput: false,
  handleActivePayType: () => {},
  onChange: () => {},
  handleActiveInput: () => {},
  checkInput: () => {},
};

CardPayFieald.propTypes = {
  total: PropTypes.string,
  infoPay: PropTypes.string,
  usdDueOn: PropTypes.string,
  date: PropTypes.string,
  card: PropTypes.string,
  bank: PropTypes.string,
  value: PropTypes.string,
  cardNumber: PropTypes.string,
  mmYY: PropTypes.string,
  cvc: PropTypes.string,
  payInvoice: PropTypes.string,
  activePayType: PropTypes.bool,
  activeInput: PropTypes.bool,
  handleActivePayType: PropTypes.func,
  onChange: PropTypes.func,
  handleActiveInput: PropTypes.func,
  checkInput: PropTypes.func,
};

export default CardPayFieald;
