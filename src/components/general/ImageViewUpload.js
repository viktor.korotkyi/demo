import PropTypes from "prop-types";
import Cropper from "react-easy-crop";
import styles from "../../../scss/components/general/ImageViewUpload.module.scss";
import Button from "../UI/Button";

const ImageViewUpload = ({
  src,
  deleteUploadFile,
  id,
  handleCroppedImage,
  settingsCrop,
  onCropChange,
  onCropComplete,
  onZoomChange,
  onRotationChange,
  rotation,
  rotateLeft,
  rotateRight,
}) => (
  <div className={styles.container}>
    <div className={styles.imageContainer}>
      <Cropper
        image={src}
        crop={settingsCrop.crop}
        zoom={settingsCrop.zoom}
        aspect={settingsCrop.aspect}
        onRotationChange={onRotationChange}
        onCropChange={onCropChange}
        onCropComplete={onCropComplete}
        onZoomChange={onZoomChange}
        rotation={rotation}
      />
    </div>
    <div className={styles.btnPanel}>
      <Button color="btnImageViewUpload" id="scale">
        <img src="/static/images/icon/icon1.png" alt="btn" />
      </Button>
      <Button color="btnImageViewUpload" onClick={() => handleCroppedImage(id)}>
        <img src="/static/images/icon/icon2.png" alt="btn" />
      </Button>
      <Button color="btnImageViewUpload" onClick={() => rotateLeft(id)}>
        <img src="/static/images/icon/icon3.png" alt="btn" />
      </Button>
      <Button color="btnImageViewUpload" onClick={() => rotateRight(id)}>
        <img src="/static/images/icon/icon4.png" alt="btn" />
      </Button>
      <Button color="btnImageViewUpload" onClick={() => deleteUploadFile(id)}>
        <img src="/static/images/icon/icon5.png" alt="btn" />
      </Button>
    </div>
  </div>
);

ImageViewUpload.defaultProps = {
  src: "",
  id: "",
  deleteUploadFile: () => {},
  handleCroppedImage: () => {},
  settingsCrop: {},
  onCropChange: () => {},
  onCropComplete: () => {},
  onZoomChange: () => {},
  onRotationChange: () => {},
  rotation: 0,
  rotateLeft: () => {},
  rotateRight: () => {},
};

ImageViewUpload.propTypes = {
  src: PropTypes.string,
  id: PropTypes.string,
  deleteUploadFile: PropTypes.func,
  handleCroppedImage: PropTypes.func,
  settingsCrop: PropTypes.shape({
    crop: PropTypes.shape({}),
    zoom: PropTypes.number,
    aspect: PropTypes.number,
  }),
  onCropChange: PropTypes.func,
  onCropComplete: PropTypes.func,
  onZoomChange: PropTypes.func,
  onRotationChange: PropTypes.func,
  rotation: PropTypes.number,
  rotateLeft: PropTypes.func,
  rotateRight: PropTypes.func,
};

export default ImageViewUpload;
