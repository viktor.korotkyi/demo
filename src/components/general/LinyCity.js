import PropTypes from "prop-types";
import Select from "../UI/Select";
import styles from "../../../scss/components/general/LineCity.module.scss";

const LineCity = ({
  form,
  handleForm,
  state,
  city,
  placeholderCity,
  placeholderState,
  chooseLabel,
  labelFirst,
  labelSecond,
}) => (
  <div className={styles.container}>
    <Select
      name="city"
      label={labelFirst}
      placeholder={placeholderCity}
      onChange={handleForm}
      list={city}
      value={form.city}
      chooseLabel={chooseLabel}
      allWidth
    />
    <Select
      name="state"
      label={labelSecond}
      placeholder={placeholderState}
      onChange={handleForm}
      list={state}
      value={form.state}
      chooseLabel={placeholderState}
    />
  </div>
);

LineCity.defaultProps = {
  handleForm: () => {},
  form: {},
  state: [],
  city: [],
  placeholderCity: "",
  placeholderState: "",
  chooseLabel: "",
  labelFirst: "",
  labelSecond: "",
};

LineCity.propTypes = {
  handleForm: PropTypes.func,
  form: PropTypes.shape({
    city: PropTypes.string,
    state: PropTypes.string,
  }),
  state: PropTypes.arrayOf(PropTypes.string),
  city: PropTypes.arrayOf(PropTypes.string),
  placeholderCity: PropTypes.string,
  placeholderState: PropTypes.string,
  chooseLabel: PropTypes.string,
  labelFirst: PropTypes.string,
  labelSecond: PropTypes.string,
};

export default LineCity;
