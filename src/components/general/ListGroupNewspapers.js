import PropTypes from "prop-types";
import CardNewspapers from "./CardNewspapers";

import styles from "../../../scss/components/general/ListGroupNewspapers.module.scss";

const ListGroupNewspapers = ({
  header,
  infoGroup,
  list,
  createPub,
  btnName,
  hideBtn,
  listView,
  handleGroup,
  group,
}) => (
  <div className={styles.container}>
    <h3 className={styles.header}>{header}</h3>
    <span className={styles.info}>{infoGroup}</span>
    <div className={listView ? styles.containerCards : styles.containerCardsGallery}>
      {list &&
        list.map((item, index) => (
          <CardNewspapers
            key={[index, item.name].join("_")}
            header={item.name}
            img={item.logo}
            pricing={item.tagLine}
            info={item.description}
            btnName={btnName}
            createPub={createPub}
            hideBtn={hideBtn}
            listView={listView}
            handleGroup={handleGroup}
            checkCard={group[item.name]}
          />
        ))}
    </div>
  </div>
);

ListGroupNewspapers.defaultProps = {
  header: "",
  infoGroup: "",
  btnName: "",
  list: [],
  hideBtn: false,
  createPub: () => {},
  handleGroup: () => {},
  group: {},
  listView: true,
};

ListGroupNewspapers.propTypes = {
  header: PropTypes.string,
  infoGroup: PropTypes.string,
  btnName: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({})),
  createPub: PropTypes.func,
  hideBtn: PropTypes.bool,
  listView: PropTypes.bool,
  handleGroup: PropTypes.func,
  group: PropTypes.shape({}),
};

export default ListGroupNewspapers;
