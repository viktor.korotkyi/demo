import PropTypes from "prop-types";
import styles from "../../../scss/components/general/PrintSocialView.module.scss";
import Button from "../UI/Button";

const PrintSocialView = ({ headerPrint, editPrint, edit, srcImg }) => (
  <div className={styles.container}>
    <div className={styles.header}>
      <span>{headerPrint}</span>
      <Button color="buttonTransparentRed" onClick={editPrint}>
        {edit}
      </Button>
    </div>
    <div className={styles.imgContainer}>
      <img src={srcImg} alt="img" />
    </div>
  </div>
);

PrintSocialView.defaultProps = {
  headerPrint: "",
  editPrint: () => {},
  edit: "",
  srcImg: "",
};

PrintSocialView.propTypes = {
  headerPrint: PropTypes.string,
  editPrint: PropTypes.func,
  edit: PropTypes.string,
  srcImg: PropTypes.string,
};

export default PrintSocialView;
