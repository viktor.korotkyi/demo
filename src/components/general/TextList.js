import PropTypes from "prop-types";
import styles from "../../../scss/components/general/TextList.module.scss";

const TextList = ({ title, selectedItem }) => (
  <div className={styles.textInfo}>
    <span>{`${title} ${selectedItem.title}:`}</span>
    {selectedItem.item.length > 0 &&
      selectedItem.item.map((el, index) => <span key={[el, index].join("_")}>{el}</span>)}
  </div>
);

TextList.defaultProps = {
  title: "",
  selectedItem: {},
};

TextList.propTypes = {
  title: PropTypes.string,
  selectedItem: PropTypes.shape({
    title: PropTypes.string,
    item: PropTypes.arrayOf(PropTypes.string),
  }),
};

export default TextList;
