import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import styles from "../../../scss/components/general/ModalDate.module.scss";
import ModalLayout from "../../layouts/ModalLayout";
import CloseButton from "./CloseButton";
import DatePickerBar from "./DatePickerBar";
import Button from "../UI/Button";

const ModalDate = ({
  closeModal,
  headerFromDate,
  headerToDate,
  handleChooseDateFrom,
  handleChooseDateTo,
  handleYearDate,
  openCalendar,
  calendarStatus,
  chooseDate,
  addButtonDate,
  addDate,
  textBtdd,
  remove,
}) => (
  <ModalLayout typeContainer="template">
    <div className={styles.container}>
      <div className={styles.btnBox}>
        <div className={styles.header}>
          <span>date</span>
          <FontAwesomeIcon icon={faCalendar} className="fa-fw" />
        </div>
        <CloseButton click={closeModal} closeType="closeTemplate" />
      </div>
      <div className={styles.dateBox}>
        <DatePickerBar
          header={headerFromDate}
          handleYearDate={handleYearDate}
          id={headerFromDate}
          openCalendar={openCalendar}
          calendarStatus={calendarStatus.from}
          onChange={handleChooseDateFrom}
          valueDate={chooseDate.from.date}
          valueYear={chooseDate.from.year}
        />
        {addButtonDate && (
          <div className={styles.dateAdd}>
            <DatePickerBar
              header={headerToDate}
              handleYearDate={handleYearDate}
              id={headerToDate}
              openCalendar={openCalendar}
              calendarStatus={calendarStatus.to}
              onChange={handleChooseDateTo}
              valueDate={chooseDate.to.date}
              valueYear={chooseDate.to.year}
            />
            <Button color="addDate" onClick={addDate}>
              {remove}
            </Button>
          </div>
        )}
        {!addButtonDate && (
          <Button color="addDate" onClick={addDate}>
            {textBtdd}
          </Button>
        )}
      </div>
    </div>
  </ModalLayout>
);

ModalDate.defaultProps = {
  closeModal: () => {},
  headerFromDate: "",
  headerToDate: "",
  textBtdd: "",
  remove: "",
  handleChooseDateFrom: () => {},
  handleChooseDateTo: () => {},
  handleYearDate: () => {},
  openCalendar: () => {},
  addButtonDate: false,
  addDate: () => {},
  calendarStatus: {},
  chooseDate: {},
};

ModalDate.propTypes = {
  closeModal: PropTypes.func,
  headerFromDate: PropTypes.string,
  headerToDate: PropTypes.string,
  textBtdd: PropTypes.string,
  remove: PropTypes.string,
  handleYearDate: PropTypes.func,
  openCalendar: PropTypes.func,
  handleChooseDateFrom: PropTypes.func,
  handleChooseDateTo: PropTypes.func,
  calendarStatus: PropTypes.shape({
    from: PropTypes.bool,
    to: PropTypes.bool,
  }),
  chooseDate: PropTypes.shape({
    from: PropTypes.shape({
      date: PropTypes.string,
      year: PropTypes.string,
    }),
    to: PropTypes.shape({
      date: PropTypes.string,
      year: PropTypes.string,
    }),
  }),
  addButtonDate: PropTypes.bool,
  addDate: PropTypes.func,
};

export default ModalDate;
