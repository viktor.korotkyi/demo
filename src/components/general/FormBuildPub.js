import PropTypes from "prop-types";
import styles from "../../../scss/components/general/FormBuildPub.module.scss";
import DateRange from "./DateRange";
import FormImagesPub from "./FormImagesPub";
import FormInputsPub from "./FormInputsPub";

const FormBuildPub = ({
  headerDate,
  textFirst,
  textSecond,
  dateFirst,
  dateSecond,
  openModalDate,
  uploadFile,
  handleTextPub,
  hideChooseTarget,
  placeholderSelect,
  titleSelect,
  listSelect,
  handleSelect,
  deleteUploadFile,
  handleCroppedImage,
  settingsCrop,
  onCropChange,
  onCropComplete,
  onZoomChange,
  onRotationChange,
  rotation,
  rotateLeft,
  rotateRight,
  choosedTemplate,
  imageSrc,
}) => {
  return (
    <form className={`${styles.container} ${styles.social}`}>
      <div className={styles.scroll}>
        {choosedTemplate.elements.map((item, index) => {
          if (item.type === "date") {
            return (
              <DateRange
                key={[item.id, index].join("_")}
                header={headerDate}
                textFirst={textFirst}
                textSecond={textSecond}
                dateFirst={dateFirst}
                dateSecond={dateSecond}
                openModalDate={openModalDate}
              />
            );
          }
          return null;
        })}
        <FormImagesPub
          imageSrc={imageSrc}
          arrayInputs={choosedTemplate.elements}
          uploadFile={uploadFile}
          deleteUploadFile={deleteUploadFile}
          rotateLeft={rotateLeft}
          rotateRight={rotateRight}
          onRotationChange={onRotationChange}
          rotation={rotation}
          handleCroppedImage={handleCroppedImage}
          settingsCrop={settingsCrop}
          onCropChange={onCropChange}
          onCropComplete={onCropComplete}
          onZoomChange={onZoomChange}
        />
        <FormInputsPub arrayInputs={choosedTemplate.elements} handleTextPub={handleTextPub} />
      </div>
    </form>
  );
};

FormBuildPub.defaultProps = {
  headerDate: "",
  textFirst: "",
  textSecond: "",
  dateFirst: "",
  dateSecond: "",
  placeholderSelect: "",
  titleSelect: "",
  hideChooseTarget: false,
  listSelect: [],
  handleTextPub: () => {},
  openModalDate: () => {},
  uploadFile: () => {},
  handleSelect: () => {},
  deleteUploadFile: () => {},
  handleCroppedImage: () => {},
  settingsCrop: {},
  onCropChange: () => {},
  onCropComplete: () => {},
  onZoomChange: () => {},
  onRotationChange: () => {},
  rotation: 0,
  rotateLeft: () => {},
  rotateRight: () => {},
  choosedTemplate: {},
  imageSrc: "",
};

FormBuildPub.propTypes = {
  headerDate: PropTypes.string,
  textFirst: PropTypes.string,
  textSecond: PropTypes.string,
  dateFirst: PropTypes.string,
  dateSecond: PropTypes.string,
  placeholderSelect: PropTypes.string,
  titleSelect: PropTypes.string,
  uploadFile: PropTypes.func,
  openModalDate: PropTypes.func,
  handleTextPub: PropTypes.func,
  handleSelect: PropTypes.func,
  deleteUploadFile: PropTypes.func,
  hideChooseTarget: PropTypes.bool,
  listSelect: PropTypes.arrayOf(PropTypes.shape({})),
  handleCroppedImage: PropTypes.func,
  settingsCrop: PropTypes.shape({}),
  onCropChange: PropTypes.func,
  onCropComplete: PropTypes.func,
  onZoomChange: PropTypes.func,
  onRotationChange: PropTypes.func,
  rotation: PropTypes.number,
  rotateLeft: PropTypes.func,
  rotateRight: PropTypes.func,
  choosedTemplate: PropTypes.shape({
    elements: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  imageSrc: PropTypes.string,
};

export default FormBuildPub;
