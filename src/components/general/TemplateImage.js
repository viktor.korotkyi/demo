import PropTypes from "prop-types";
import constants from "../../constants/storageWorks";
import { filterArray, splitStyleSize } from "../../utils/storageWorks";

const TemplateImage = ({ item, url, urlDefault, image }) => {
  const styleSettings = {
    width: item.width,
    height: item.height,
    fontFamily: item.fontFamily,
    y: item.y,
    x: item.x,
    alignSelf: item.alignment,
    marginTop: Number(splitStyleSize(filterArray(item.margin, constants.marginTop)[0].value)),
    marginRight: Number(splitStyleSize(filterArray(item.margin, constants.marginRight)[0].value)),
    marginLeft: Number(splitStyleSize(filterArray(item.margin, constants.marginLeft)[0].value)),
    marginBottom: Number(splitStyleSize(filterArray(item.margin, constants.marginBottom)[0].value)),
  };
  return <img style={styleSettings} src={image || url} alt={constants.image} />;
};

TemplateImage.defaultProps = {
  item: {},
  url: "",
  urlDefault: "",
};

TemplateImage.propTypes = {
  url: PropTypes.string,
  urlDefault: PropTypes.string,
  item: PropTypes.shape({
    id: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    y: PropTypes.number,
    x: PropTypes.number,
    fontFamily: PropTypes.string,
    textAlign: PropTypes.string,
    type: PropTypes.string,
    textDecoration: PropTypes.string,
    fontWeight: PropTypes.string,
    fontStyle: PropTypes.string,
    fontSize: PropTypes.string,
    alignment: PropTypes.string,
    margin: PropTypes.arrayOf(PropTypes.shape({})),
  }),
};

export default TemplateImage;
