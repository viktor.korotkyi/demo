import PropTypes from "prop-types";
import NewsPaperMini from "./NewsPaperMini";
import TextBtnOrderDetails from "./TextBtnOrderDetails";
import styles from "../../../scss/components/general/NewspaperBar.module.scss";
import BoxDate from "./BoxDate";
import { getMonthTitle } from "../../utils/storageWorks";

const NewspaperBar = ({
  open,
  openStatus,
  btnText,
  img,
  newspaper,
  yourPublicationDates,
  editPubDate,
  newspaperUnder,
  id,
  date,
}) => (
  <div className={styles.container}>
    <NewsPaperMini
      imgPath={img}
      newspaper={newspaper}
      open={open}
      newspaperUnder={newspaperUnder}
      id={id}
    />
    {openStatus && (
      <div className={styles.containerDatesPub}>
        <TextBtnOrderDetails btnText={btnText} text={yourPublicationDates} onClick={editPubDate} />
        <div className={styles.dateBox}>
          {date.map((item, index) => (
            <BoxDate
              key={[item, index].join("_")}
              date={`${item}`.split(".")[2]}
              month={getMonthTitle(`${item}`.split(".")[0], `${item}`.split(".")[1])}
            />
          ))}
        </div>
      </div>
    )}
  </div>
);

NewspaperBar.defaultProps = {
  btnText: "",
  img: "",
  newspaper: "",
  yourPublicationDates: "",
  newspaperUnder: "",
  id: "",
  editPubDate: () => {},
  open: () => {},
  openStatus: false,
  date: [],
};

NewspaperBar.propTypes = {
  btnText: PropTypes.string,
  img: PropTypes.string,
  newspaper: PropTypes.string,
  open: PropTypes.func,
  yourPublicationDates: PropTypes.string,
  editPubDate: PropTypes.func,
  openStatus: PropTypes.bool,
  newspaperUnder: PropTypes.string,
  id: PropTypes.string,
  date: PropTypes.arrayOf(PropTypes.string),
};

export default NewspaperBar;
