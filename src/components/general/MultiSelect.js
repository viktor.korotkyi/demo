import PropTypes from "prop-types";
import Select from "react-select";

const MultiSelect = ({ placeholder, list, handleSelect }) => {
  const customStyles = {
    option: (provided, { data }) => ({
      ...provided,
      fontSize: "16px",
      fontWeight: "500",
      color: "#898989",
      backgroundColor: data.id % 2 !== 0 ? "#ffffff" : "rgba(45, 45, 45, 0.1)",
      cursor: "pointer",
      "&:hover": {
        backgroundColor: "#000000",
        color: "#ffffff",
      },
    }),
    control: (provided) => ({
      ...provided,
      marginTop: "10px",
      border: "solid 2px #898989",
      borderRadius: "0",
      cursor: "pointer",
      padding: "0 0",
      boxShadow: 0,
      "&:hover": {
        border: "solid 2px rgba(45, 45, 45, 0.75)",
      },
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: "16px",
      color: "#898989",
      textTransform: "capitalize",
    }),
    clearIndicator: (provided) => ({
      ...provided,
      display: "none",
    }),
    dropdownIndicator: (provided) => ({
      ...provided,
      display: "none",
    }),
    multiValue: (provided) => ({
      ...provided,
      backgroundColor: "rgba(0, 68, 137, 0.1)",
      border: "solid 1px #004489",
      borderRadius: "0",
    }),
    multiValueLabel: (provided) => ({
      ...provided,
      fontSize: "14px",
      fontWeight: "bold",
      letterSpacing: "1px",
      color: "#004489",
    }),
    multiValueRemove: (provided) => ({
      ...provided,
      color: "#004489",
      "&:hover": {
        backgroundColor: "transparent",
        color: "#004489",
      },
    }),

    indicatorSeparator: (provided) => ({
      ...provided,
      display: "none",
    }),
    menu: (provided) => ({
      ...provided,
      borderRadius: "0",
      border: "solid 2px rgba(45, 45, 45, 0.75)",
      backgroundColor: "#f4f4f4",
      marginTop: "-2px",
    }),
    menuList: (provided) => ({
      ...provided,
      padding: "0",
    }),
    input: (provided) => ({
      ...provided,
      height: "45px",
      margin: "-5px",
      padding: "10px 0 0",
    }),
    valueContainer: (provided) => ({
      ...provided,
      padding: "3px 0",
    }),
  };

  return (
    <Select
      styles={customStyles}
      options={list}
      placeholder={<div>{placeholder}</div>}
      onChange={handleSelect}
      isMulti
    />
  );
};

MultiSelect.defaultProps = {
  placeholder: "",
  list: [],
  handleSelect: () => {},
};

MultiSelect.propTypes = {
  placeholder: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({})),
  handleSelect: PropTypes.func,
};

export default MultiSelect;
