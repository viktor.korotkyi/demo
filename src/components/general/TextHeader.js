import PropTypes from "prop-types";
import constants from "../../constants/storageWorks";
import fontsType from "../../constants/fontsType";
import { filterArray, splitStyleSize } from "../../utils/storageWorks";

const TextHeader = ({ item, text }) => {
  const styleSettings = {
    fontFamily: `${item.fontFamily}, sans-serif`,
    fontSize: Number(item.fontSize),
    fontStyle: item.fontStyle,
    fontWeight: item.fontWeight,
    y: item.y,
    x: item.x,
    alignSelf: item.textAlign,
    textAlign: item.textAlign,
    textDecoration: item.textDecoration,
    marginTop: Number(splitStyleSize(filterArray(item.margin, constants.marginTop)[0].value)),
    // marginRight: Number(splitStyleSize(filterArray(item.margin, constants.marginRight)[0].value)),
    // marginLeft: Number(splitStyleSize(filterArray(item.margin, constants.marginLeft)[0].value)),
    marginBottom: Number(splitStyleSize(filterArray(item.margin, constants.marginBottom)[0].value)),
  };
  const renderText = (type) => {
    switch (type) {
      case fontsType.headerOne:
        return <h1 style={styleSettings}>{text}</h1>;
      case fontsType.headerTwo:
        return <h2 style={styleSettings}>{text}</h2>;
      case fontsType.headerThree:
        return <h3 style={styleSettings}>{text}</h3>;
      case fontsType.headerFour:
        return <h4 style={styleSettings}>{text}</h4>;
      case fontsType.headerFive:
        return <h5 style={styleSettings}>{text}</h5>;
      case fontsType.headerSix:
        return <h6 style={styleSettings}>{text}</h6>;
      default:
        return <span style={styleSettings}>{text}</span>;
    }
  };

  const tag = () => item.id?.split(".")[0];
  return renderText(tag());
};

TextHeader.defaultProps = {
  item: {},
  text: "",
};

TextHeader.propTypes = {
  item: PropTypes.shape({
    text: PropTypes.string,
    y: PropTypes.number,
    x: PropTypes.number,
    fontFamily: PropTypes.string,
    textAlign: PropTypes.string,
    type: PropTypes.string,
    textDecoration: PropTypes.string,
    fontWeight: PropTypes.string,
    fontStyle: PropTypes.string,
    id: PropTypes.string,
    fontSize: PropTypes.number,
    margin: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  text: PropTypes.string,
};

export default TextHeader;
