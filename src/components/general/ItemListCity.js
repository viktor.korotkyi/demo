import PropTypes from "prop-types";

import styles from "../../../scss/components/general/ItemListCity.module.scss";

const ItemListCity = ({ city, number }) => (
  <div className={styles.container}>
    <div>{number}</div>
    <span>{city}</span>
  </div>
);

ItemListCity.defaultProps = {
  city: "",
  number: "",
};

ItemListCity.propTypes = {
  city: PropTypes.string,
  number: PropTypes.string,
};

export default ItemListCity;
