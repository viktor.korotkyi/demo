import PropTypes from "prop-types";
import Button from "../UI/Button";
import styles from "../../../scss/components/general/TextBtnOrderDetails.module.scss";

const TextBtnOrderDetails = ({ text, btnText, onClick }) => (
  <div className={styles.container}>
    <span>{text}</span>
    <Button color="buttonTransparentColor" onClick={onClick}>
      {btnText}
    </Button>
  </div>
);

TextBtnOrderDetails.defaultProps = {
  text: "",
  btnText: "",
  onClick: () => {},
};

TextBtnOrderDetails.propTypes = {
  text: PropTypes.string,
  btnText: PropTypes.string,
  onClick: PropTypes.func,
};

export default TextBtnOrderDetails;
