import PropTypes from "prop-types";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Loader from "./Loader";
import styles from "../../../scss/components/general/StatusIcon.module.scss";

const StatusIcon = ({ status, visible }) => (
  <div className={styles.statusIcon}>
    {visible && (
      <>
        {status && <FontAwesomeIcon icon={faCheck} />}
        {!status && <Loader />}
      </>
    )}
  </div>
);

StatusIcon.defaultProps = {
  status: false,
  visible: true,
};

StatusIcon.propTypes = {
  status: PropTypes.bool,
  visible: PropTypes.bool,
};

export default StatusIcon;
