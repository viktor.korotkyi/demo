import PropTypes from "prop-types";
import ImageUpload from "./ImageUpload";
import constants from "../../constants/storageWorks";
import styles from "../../../scss/components/general/FormImagesPub.module.scss";

const FormImagesPub = ({
  arrayInputs,
  uploadFile,
  deleteUploadFile,
  handleCroppedImage,
  settingsCrop,
  onCropChange,
  onCropComplete,
  onZoomChange,
  onRotationChange,
  rotation,
  rotateLeft,
  rotateRight,
  imageSrc,
}) => (
  <div className={styles.imageBox}>
    {arrayInputs.map(
      (item, index) =>
        item.type === constants.image && (
          <ImageUpload
            key={[index, item.id].join("_")}
            title="Upload Image"
            titleRight=".jpeg or .png"
            uploadFile={uploadFile}
            fileUpload={imageSrc}
            id={item.id}
            deleteUploadFile={deleteUploadFile}
            handleCroppedImage={handleCroppedImage}
            settingsCrop={settingsCrop}
            onCropChange={onCropChange}
            onCropComplete={onCropComplete}
            onZoomChange={onZoomChange}
            onRotationChange={onRotationChange}
            rotation={rotation}
            rotateLeft={rotateLeft}
            rotateRight={rotateRight}
          />
        )
    )}
  </div>
);

FormImagesPub.defaultProps = {
  imageSrc: "",
  arrayInputs: [],
  uploadFile: () => {},
  deleteUploadFile: () => {},
  handleCroppedImage: () => {},
  settingsCrop: {},
  onCropChange: () => {},
  onCropComplete: () => {},
  onZoomChange: () => {},
  onRotationChange: () => {},
  rotation: 0,
  rotateLeft: () => {},
  rotateRight: () => {},
};

FormImagesPub.propTypes = {
  imageSrc: PropTypes.string,
  uploadFile: PropTypes.func,
  deleteUploadFile: PropTypes.func,
  arrayInputs: PropTypes.arrayOf(PropTypes.shape({})),
  handleCroppedImage: PropTypes.func,
  settingsCrop: PropTypes.shape({}),
  onCropChange: PropTypes.func,
  onCropComplete: PropTypes.func,
  onZoomChange: PropTypes.func,
  onRotationChange: PropTypes.func,
  rotation: PropTypes.number,
  rotateLeft: PropTypes.func,
  rotateRight: PropTypes.func,
};

export default FormImagesPub;
