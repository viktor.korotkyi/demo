import PropTypes from "prop-types";
import Button from "../UI/Button";
import ItemPrice from "./ItemPrice";
import styles from "../../../scss/components/general/PricingBoxOrder.module.scss";

const PricingBoxOrder = ({
  priceList,
  total,
  btnDawnLoadPrice,
  stylePrice,
  hideBtn,
  packageItem,
  facebook,
}) => (
  <div className={`${styles.container} ${stylePrice && styles[stylePrice]}`}>
    <div className={styles.itemPrice}>
      {priceList &&
        priceList.map((item, index) => (
          <ItemPrice key={[item.name, index].join("_")} item={item.name} price={`${item.price}`} />
        ))}
      <ItemPrice item={packageItem.name} price={`${packageItem.price}`} />
      <ItemPrice item={facebook.name} price={`${facebook.price}`} />
    </div>
    <div className={styles.total}>
      <ItemPrice
        item={total}
        price={
          priceList.length > 0
            ? `${
                priceList.map((item) => Number(item.price)).reduce((sum, amount) => sum + amount) +
                packageItem.price +
                facebook.price
              }`
            : `${priceList.length}`
        }
        styleType="bold"
      />
    </div>
    {!hideBtn && <Button color="buttonTransparentRed">{btnDawnLoadPrice}</Button>}
  </div>
);

PricingBoxOrder.defaultProps = {
  priceList: [],
  packageItem: {},
  facebook: {},
  total: "",
  btnDawnLoadPrice: "",
  stylePrice: "",
  hideBtn: false,
};

PricingBoxOrder.propTypes = {
  priceList: PropTypes.arrayOf(
    PropTypes.shape({
      cost: PropTypes.any,
    })
  ),
  total: PropTypes.string,
  btnDawnLoadPrice: PropTypes.string,
  stylePrice: PropTypes.string,
  hideBtn: PropTypes.bool,
  packageItem: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
  }),
  facebook: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
  }),
};

export default PricingBoxOrder;
