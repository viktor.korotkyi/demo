import PropTypes from "prop-types";
import DayPicker from "react-day-picker";
import Input from "../UI/Input";
import styles from "../../../scss/components/general/DatePickerBar.module.scss";
import Button from "../UI/Button";

const DatePickerBar = ({
  header,
  id,
  valueDate,
  valueYear,
  handleYearDate,
  onChange,
  openCalendar,
  calendarStatus,
}) => (
  <div className={styles.containerDate}>
    <span className={styles.header}>{header}</span>
    <div className={styles.inputContainer}>
      <Button color={valueDate !== "" ? "choosed" : "chooseDate"} onClick={() => openCalendar(id)}>
        {valueDate !== "" ? valueDate : "Date"}
      </Button>
      <Input
        placeholder="Year"
        id={id}
        onChange={handleYearDate}
        value={valueYear}
        styleType={valueYear !== "" ? "choosed" : "chooseYear"}
        autocomplete="off"
      />
    </div>
    {calendarStatus && (
      <div className={styles.calendar}>
        <DayPicker onDayClick={onChange} classNames={styles} />
      </div>
    )}
  </div>
);

DatePickerBar.defaultProps = {
  header: "",
  valueDate: "",
  valueYear: "",
  id: "",
  handleYearDate: () => {},
  onChange: () => {},
  openCalendar: () => {},
  calendarStatus: false,
};

DatePickerBar.propTypes = {
  header: PropTypes.string,
  valueDate: PropTypes.string,
  valueYear: PropTypes.string,
  id: PropTypes.string,
  handleYearDate: PropTypes.func,
  onChange: PropTypes.func,
  openCalendar: PropTypes.func,
  calendarStatus: PropTypes.bool,
};

export default DatePickerBar;
