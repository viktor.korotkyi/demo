import PropTypes from "prop-types";
import constans from "../../constants/storageWorks";
import fontsType from "../../constants/fontsType";
import { filterArray } from "../../utils/storageWorks";

const TextDatePrevious = ({ item, chooseDate }) => {
  const styleSettings = {
    fontFamily: `${item.fontFamily}, sans-serif`,
    fontSize: Number(item.fontSize),
    fontStyle: item.fontStyle,
    fontWeight: item.fontWeight,
    y: item.y,
    x: item.x,
    alignSelf: item.textAlign,
    textDecoration: item.textDecoration,
    marginTop: Number(filterArray(item.margin, constans.marginTop)[0].value),
    marginRight: Number(filterArray(item.margin, constans.marginRight)[0].value),
    marginLeft: Number(filterArray(item.margin, constans.marginLeft)[0].value),
    marginBottom: Number(filterArray(item.margin, constans.marginBottom)[0].value),
    textAlign: "center",
  };

  const textDate = `
    ${chooseDate.from.date},
    ${chooseDate.from.year} -
    ${chooseDate.to.date},
    ${chooseDate.to.year}
  `;

  const renderText = (type) => {
    switch (type) {
      case fontsType.headerOne:
        return <h1 style={styleSettings}>{textDate}</h1>;
      case fontsType.headerTwo:
        return <h2 style={styleSettings}>{textDate}</h2>;
      case fontsType.headerThree:
        return <h3 style={styleSettings}>{textDate}</h3>;
      case fontsType.headerFour:
        return <h4 style={styleSettings}>{textDate}</h4>;
      case fontsType.headerFive:
        return <h5 style={styleSettings}>{textDate}</h5>;
      case fontsType.headerSix:
        return <h6 style={styleSettings}>{textDate}</h6>;
      default:
        return <span style={styleSettings}>{textDate}</span>;
    }
  };

  const tag = () => item.id?.split(".")[0];
  return <>{renderText(tag())}</>;
};

TextDatePrevious.defaultProps = {
  item: {},
  chooseDate: {},
};

TextDatePrevious.propTypes = {
  chooseDate: PropTypes.shape({
    from: PropTypes.shape({
      date: PropTypes.string,
      year: PropTypes.string,
    }),
    to: PropTypes.shape({
      date: PropTypes.string,
      year: PropTypes.string,
    }),
  }),
  item: PropTypes.shape({
    text: PropTypes.string,
    y: PropTypes.number,
    x: PropTypes.number,
    fontFamily: PropTypes.string,
    textAlign: PropTypes.string,
    type: PropTypes.string,
    textDecoration: PropTypes.string,
    fontWeight: PropTypes.string,
    fontStyle: PropTypes.string,
    id: PropTypes.string,
    fontSize: PropTypes.number,
    margin: PropTypes.arrayOf(PropTypes.shape({})),
  }),
};

export default TextDatePrevious;
