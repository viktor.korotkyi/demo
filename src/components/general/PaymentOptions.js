import PropTypes from "prop-types";
import Button from "../UI/Button";
import styles from "../../../scss/components/general/PaymentOptions.module.scss";

const PaymentOptions = ({ header, firstBtn, secondBtn, firstBtnClick, secondBtnClick }) => (
  <div className={styles.container}>
    <span>{header}</span>
    <Button color="buttonTransparentRed" onClick={firstBtnClick}>
      {firstBtn}
    </Button>
    <Button color="buttonTransparentRed" onClick={secondBtnClick}>
      {secondBtn}
    </Button>
  </div>
);

PaymentOptions.defaultProps = {
  header: "",
  firstBtn: "",
  secondBtn: "",
  firstBtnClick: () => {},
  secondBtnClick: () => {},
};

PaymentOptions.propTypes = {
  header: PropTypes.string,
  firstBtn: PropTypes.string,
  secondBtn: PropTypes.string,
  firstBtnClick: PropTypes.func,
  secondBtnClick: PropTypes.func,
};

export default PaymentOptions;
