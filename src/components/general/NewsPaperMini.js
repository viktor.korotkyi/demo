import PropTypes from "prop-types";
import Button from "../UI/Button";

const NewsPaperMini = ({ imgPath, newspaper, newspaperUnder, open, id }) => (
  <Button color="btnCardPubMini" onClick={() => open(id)}>
    <img src={imgPath} alt="logo" />
    <div>
      <span>{newspaper}</span>
      <span>{newspaperUnder}</span>
    </div>
  </Button>
);

NewsPaperMini.defaultProps = {
  imgPath: "",
  newspaper: "",
  newspaperUnder: "",
  id: "",
  open: () => {},
};

NewsPaperMini.propTypes = {
  imgPath: PropTypes.string,
  newspaper: PropTypes.string,
  newspaperUnder: PropTypes.string,
  id: PropTypes.string,
  open: PropTypes.func,
};

export default NewsPaperMini;
