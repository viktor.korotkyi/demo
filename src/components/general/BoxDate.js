import PropTypes from "prop-types";

import styles from "../../../scss/components/general/BoxDate.module.scss";

const BoxDate = ({ date, month }) => (
  <div className={styles.container}>
    <h3>{date}</h3>
    <span>{month}</span>
  </div>
);

BoxDate.defaultProps = {
  date: "",
  month: "",
};

BoxDate.propTypes = {
  date: PropTypes.string,
  month: PropTypes.string,
};

export default BoxDate;
