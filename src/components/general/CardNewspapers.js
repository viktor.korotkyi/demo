import PropTypes from "prop-types";
import Button from "../UI/Button";
import styles from "../../../scss/components/general/CardNewspapers.module.scss";

const CardNewspapers = ({
  header,
  pricing,
  img,
  info,
  btnName,
  createPub,
  hideBtn,
  listView,
  handleGroup,
  checkCard,
}) => (
  <div
    className={`${styles.container} ${!listView && styles.compactCards} ${
      checkCard && styles.active
    }`}
    onClick={() => handleGroup(header)}
  >
    <div className={styles.contentBox}>
      <img src={img} alt="logoNew" />
      <div className={styles.pricingBox}>
        <h3>{header}</h3>
        <span>{pricing}</span>
        <span className={`${listView && styles.hide}`}>{info}</span>
      </div>
      <Button color={hideBtn ? "hide" : "buttonCard"} onClick={() => createPub(header)}>
        {btnName}
      </Button>
    </div>
    <span className={`${!listView && styles.hideMain}`}>{info}</span>
    <div className={styles.btnCardMobile}>
      <Button color={hideBtn ? "hide" : "buttonCardMobile"} onClick={createPub} id={header}>
        {btnName}
      </Button>
    </div>
  </div>
);

CardNewspapers.defaultProps = {
  header: "",
  pricing: "",
  img: "",
  info: "",
  btnName: "",
  hideBtn: false,
  createPub: () => {},
  handleGroup: () => {},
  listView: true,
  checkCard: false,
};

CardNewspapers.propTypes = {
  header: PropTypes.string,
  pricing: PropTypes.string,
  img: PropTypes.string,
  info: PropTypes.string,
  btnName: PropTypes.string,
  createPub: PropTypes.func,
  handleGroup: PropTypes.func,
  hideBtn: PropTypes.bool,
  listView: PropTypes.bool,
  checkCard: PropTypes.bool,
};

export default CardNewspapers;
