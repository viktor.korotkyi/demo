import PropTypes from "prop-types";
import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import styles from "../../../scss/components/general/LinkRef.module.scss";

const LinkRef = ({ children, typeStyle, href, asPath, dashboard }) => {
  const router = useRouter();
  const active = router.asPath === asPath ? "activeLink" : "";
  const activeDashboard =
    dashboard === true && router.asPath === asPath ? "activeLinkDashboard" : "";
  return (
    <Link href={href} as={asPath} passHref>
      <span
        className={`${styles[typeStyle]} ${active && styles[active]} ${
          activeDashboard && styles[activeDashboard]
        }`}
      >
        <a>{children}</a>
      </span>
    </Link>
  );
};
LinkRef.defaultProps = {
  typeStyle: "",
  href: "",
  asPath: "",
  dashboard: false,
};

LinkRef.propTypes = {
  children: PropTypes.node.isRequired,
  typeStyle: PropTypes.string,
  href: PropTypes.string,
  asPath: PropTypes.string,
  dashboard: PropTypes.bool,
};

export default LinkRef;
