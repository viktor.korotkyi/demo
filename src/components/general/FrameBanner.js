import PropTypes from "prop-types";

import styles from "../../../scss/components/general/FrameBanner.module.scss";

const FrameBanner = ({ text, size }) => (
  <div className={styles.container}>
    <span>{size}</span>
    <span>{text}</span>
  </div>
);

FrameBanner.defaultProps = {
  text: "",
  size: "",
};

FrameBanner.propTypes = {
  text: PropTypes.string,
  size: PropTypes.string,
};

export default FrameBanner;
