import PropTypes from "prop-types";

import styles from "../../../scss/components/general/ViewPrint.module.scss";
import Button from "../UI/Button";

const ViewPrint = ({
  header,
  downloadPDF,
  downloadJPG,
  txtDawnloadPDF,
  txtDawnloadJPG,
  imgPath,
}) => (
  <div className={styles.container}>
    <div className={styles.header}>
      <span>{header}</span>
      <div className={styles.btnBox}>
        <Button color="buttonTransparentRed" onClick={downloadPDF}>
          {txtDawnloadPDF}
        </Button>
        <Button color="buttonTransparentRed" onClick={downloadJPG}>
          {txtDawnloadJPG}
        </Button>
      </div>
    </div>
    <div className={styles.imgContainer}>
      <img src={imgPath} alt="logo" />
    </div>
  </div>
);

ViewPrint.defaultProps = {
  header: "",
  txtDawnloadPDF: "",
  txtDawnloadJPG: "",
  imgPath: "",
  downloadPDF: () => {},
  downloadJPG: () => {},
};

ViewPrint.propTypes = {
  header: PropTypes.string,
  txtDawnloadPDF: PropTypes.string,
  txtDawnloadJPG: PropTypes.string,
  imgPath: PropTypes.string,
  downloadPDF: PropTypes.func,
  downloadJPG: PropTypes.func,
};

export default ViewPrint;
