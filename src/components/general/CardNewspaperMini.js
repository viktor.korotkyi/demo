import PropTypes from "prop-types";
import styles from "../../../scss/components/general/CardNewspaperMini.module.scss";

const CardNewspapers = ({ header, img, info, handleActive, checkCard }) => (
  <div
    className={`${styles.container} ${checkCard && styles.active}`}
    onClick={() => handleActive(header)}
  >
    <img src={img} alt="logoNew" id={header} />
    <div className={styles.text}>
      <h3>{header}</h3>
      <span>{info}</span>
    </div>
  </div>
);

CardNewspapers.defaultProps = {
  header: "",
  img: "",
  info: "",
  handleActive: () => {},
  checkCard: false,
};

CardNewspapers.propTypes = {
  header: PropTypes.string,
  img: PropTypes.string,
  info: PropTypes.string,
  handleActive: PropTypes.func,
  checkCard: PropTypes.bool,
};

export default CardNewspapers;
