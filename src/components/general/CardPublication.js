import PropTypes from "prop-types";
import Button from "../UI/Button";
import styles from "../../../scss/components/general/CardPublication.module.scss";

const CardPublication = ({ imgPath, order, orderNumber, textBtn, orderPlaced, adType }) => (
  <div className={styles.container}>
    <div className={styles.content}>
      <img src={imgPath} alt="logo" />
      <div className={styles.order}>
        <div>
          <span>{order}</span>
          <span>{orderNumber}</span>
        </div>
        <Button color="buttonCardPublication" onClick={adType}>
          {textBtn}
        </Button>
      </div>
    </div>
    <span className={styles.orderPlaced}>{orderPlaced}</span>
  </div>
);

CardPublication.defaultProps = {
  imgPath: "",
  order: "",
  orderNumber: "",
  textBtn: "",
  orderPlaced: "",
  adType: () => {},
};

CardPublication.propTypes = {
  imgPath: PropTypes.string,
  order: PropTypes.string,
  orderNumber: PropTypes.string,
  textBtn: PropTypes.string,
  orderPlaced: PropTypes.string,
  adType: PropTypes.func,
};

export default CardPublication;
