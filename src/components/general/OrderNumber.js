import PropTypes from "prop-types";
import Button from "../UI/Button";
import Input from "../UI/Input";
import styles from "../../../scss/components/general/OrderNumber.module.scss";

const OrderNumber = ({ header, btnText, edit, order, viewEdit, orderInfo, onChange }) => (
  <div className={styles.container}>
    <h3>{header}</h3>
    {!viewEdit && <span className={styles.order}>{`${order} ${orderInfo}`}</span>}
    {viewEdit && (
      <div className={styles.edit}>
        <span>{`${order}`}</span>
        <Input
          autocomplete="off"
          maxlength="90"
          onChange={onChange}
          value={orderInfo}
          styleType="editOrder"
          autofocus
        />
      </div>
    )}
    <Button color="btnBlueTransparent" onClick={edit}>
      {btnText}
    </Button>
  </div>
);

OrderNumber.defaultProps = {
  header: "",
  btnText: "",
  order: "",
  viewEdit: false,
  orderInfo: "",
  edit: () => {},
  onChange: () => {},
};

OrderNumber.propTypes = {
  header: PropTypes.string,
  btnText: PropTypes.string,
  order: PropTypes.string,
  orderInfo: PropTypes.string,
  viewEdit: PropTypes.bool,
  edit: PropTypes.func,
  onChange: PropTypes.func,
};

export default OrderNumber;
