import styles from "../../../scss/components/general/LogoTypographic.module.scss";

const LogoTypographic = () => (
  <div className={styles.container}>
    <img src="/static/images/Logo-typographic.png" alt="logo" />
  </div>
);

export default LogoTypographic;
