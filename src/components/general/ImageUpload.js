import PropTypes from "prop-types";
import styles from "../../../scss/components/general/ImageUpload.module.scss";
import ButtonUpload from "../UI/ButtonUpload";
import ImageViewUpload from "./ImageViewUpload";

const ImageUpload = ({
  title,
  titleRight,
  uploadFile,
  fileUpload,
  id,
  deleteUploadFile,
  handleCroppedImage,
  settingsCrop,
  onCropChange,
  onCropComplete,
  onZoomChange,
  onRotationChange,
  rotation,
  rotateLeft,
  rotateRight,
}) => (
  <div className={styles.container}>
    <div className={styles.title}>
      <span>{title}</span>
      <span>{titleRight}</span>
    </div>
    {fileUpload === "" ? (
      <ButtonUpload onClick={uploadFile} id={id} image={fileUpload} />
    ) : (
      <ImageViewUpload
        src={fileUpload}
        deleteUploadFile={deleteUploadFile}
        id={id}
        handleCroppedImage={handleCroppedImage}
        settingsCrop={settingsCrop}
        onCropChange={onCropChange}
        onCropComplete={onCropComplete}
        onZoomChange={onZoomChange}
        onRotationChange={onRotationChange}
        rotation={rotation}
        rotateLeft={rotateLeft}
        rotateRight={rotateRight}
      />
    )}
  </div>
);

ImageUpload.defaultProps = {
  id: "",
  title: "",
  titleRight: "",
  fileUpload: "",
  uploadFile: () => {},
  deleteUploadFile: () => {},
  handleCroppedImage: () => {},
  settingsCrop: {},
  onCropChange: () => {},
  onCropComplete: () => {},
  onZoomChange: () => {},
  onRotationChange: () => {},
  rotation: 0,
  rotateLeft: () => {},
  rotateRight: () => {},
};

ImageUpload.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  titleRight: PropTypes.string,
  fileUpload: PropTypes.string,
  uploadFile: PropTypes.func,
  deleteUploadFile: PropTypes.func,
  handleCroppedImage: PropTypes.func,
  settingsCrop: PropTypes.shape({}),
  onCropChange: PropTypes.func,
  onCropComplete: PropTypes.func,
  onZoomChange: PropTypes.func,
  onRotationChange: PropTypes.func,
  rotation: PropTypes.number,
  rotateLeft: PropTypes.func,
  rotateRight: PropTypes.func,
};

export default ImageUpload;
