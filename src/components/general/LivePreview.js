import PropTypes from "prop-types";
import styles from "../../../scss/components/general/LivePreview.module.scss";
import TextDatePrevious from "./TextDatePrevious";
import constants from "../../constants/storageWorks";
import TemplateImage from "./TemplateImage";
import TextHeader from "./TextHeader";
import { findIndex } from "../../utils/storageWorks";

const LivePreview = ({ chooseDate, choosedTemplate, image }) => (
  <div className={styles.container}>
    <span className={styles.preview}>Live View</span>
    <div
      className={styles.announcement}
      style={{ width: choosedTemplate.width, height: choosedTemplate.height }}
    >
      {choosedTemplate.elements.map((item, index) => {
        const indexItem = findIndex(choosedTemplate.elements, item.id);
        if (item.date === true) {
          return (
            chooseDate.from.date && (
              <TextDatePrevious
                key={[index, item.id].join("_")}
                chooseDate={chooseDate}
                item={choosedTemplate.elements[indexItem]}
              />
            )
          );
        }
        if (item.type === constants.image) {
          return (
            <TemplateImage
              key={[index, item.id].join("_")}
              item={choosedTemplate.elements[indexItem]}
              url={item.url}
              urlDefault={item.urlDefault}
              image={image}
            />
          );
        }
        return (
          <div className={styles.text}>
            <TextHeader
              key={[index, item.id].join("_")}
              item={choosedTemplate.elements[indexItem]}
              text={item.text}
            />
          </div>
        );
      })}
    </div>
  </div>
);

LivePreview.defaultProps = {
  textPub: {},
  chooseDate: {},
  template: {},
  choosedTemplate: {},
  image: "",
};

LivePreview.propTypes = {
  image: PropTypes.string,
  textPub: PropTypes.shape({
    title: PropTypes.string,
    subtitle: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string,
    imageSecond: PropTypes.string,
  }),
  chooseDate: PropTypes.shape({
    from: PropTypes.shape({
      date: PropTypes.string,
      year: PropTypes.string,
    }),
    to: PropTypes.shape({
      date: PropTypes.string,
      year: PropTypes.string,
    }),
  }),
  template: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number,
    elements: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  choosedTemplate: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number,
    elements: PropTypes.arrayOf(PropTypes.shape({})),
  }),
};

export default LivePreview;
