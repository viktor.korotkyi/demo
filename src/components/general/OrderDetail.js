import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Dot from "../UI/Dot";
import CloseButton from "./CloseButton";
import StatusBarOrder from "./StatusBarOrder";
import NewspaperBar from "./NewspaperBar";
import TextBtnOrderDetails from "./TextBtnOrderDetails";
import ViewPrint from "./ViewPrint";
import PricingBoxOrder from "./PricingBoxOrder";
import styles from "../../../scss/components/general/OrderDetail.module.scss";

const OrderDetail = ({
  t,
  orderNumber,
  close,
  submitted,
  scheduledToPublish,
  statusOrder,
  editPub,
  open,
  openStatus,
  editPubDate,
  downloadJPG,
  downloadPDF,
  priceList,
}) => (
  <div className={styles.container}>
    <div className={styles.headerOrder}>
      <div className={styles.containerOrder}>
        <h3>{t("order")}</h3>
        <h3>{orderNumber}</h3>
      </div>
      <CloseButton click={close} />
    </div>
    <div className={styles.underHeader}>
      <span>{`${t("submitted")} ${submitted}`}</span>
      <Dot typeDot="blackSmallDot" />
      <span>{`${t("scheduledToPublish")} ${scheduledToPublish}`}</span>
    </div>
    <StatusBarOrder
      statusOrder={statusOrder}
      firstStep={t("submitted")}
      secondStep={t("pendingReview")}
      lastStep={t("proofed")}
    />
    <div className={styles.boxNewsPapper}>
      <TextBtnOrderDetails btnText={t("edit")} text={t("yourChosenPub")} onClick={editPub} />

      <NewspaperBar
        id="first"
        btnText={t("edit")}
        img="/static/images/the-mercury-news.png"
        newspaper="the mercury news"
        open={open}
        editPubDate={editPubDate}
        yourPublicationDates={t("yourPublicationDates")}
        openStatus={openStatus.first}
        newspaperUnder="Includes the Piedmonter, Montclarion, Berkeley Voice and El Cerrito Journal."
      />
      <NewspaperBar
        id="second"
        btnText={t("edit")}
        img="/static/images/the-mercury-news.png"
        newspaper="hills newspapers (weekly)"
        open={open}
        editPubDate={editPubDate}
        yourPublicationDates={t("yourPublicationDates")}
        openStatus={openStatus.second}
        newspaperUnder="Includes the Piedmonter, Montclarion, Berkeley Voice and El Cerrito Journal."
      />
    </div>
    <div className={styles.boxPrint}>
      <ViewPrint
        header={t("printAd")}
        txtDawnloadJPG={t("downloadAsJPG")}
        txtDawnloadPDF={t("downloadAsPDF")}
        imgPath="/static/images/test1.png"
        downloadJPG={downloadJPG}
        downloadPDF={downloadPDF}
      />
      <ViewPrint
        header={t("socialAd")}
        txtDawnloadJPG={t("downloadAsJPG")}
        txtDawnloadPDF={t("downloadAsPDF")}
        imgPath="/static/images/test2.png"
        downloadJPG={downloadJPG}
        downloadPDF={downloadPDF}
      />
    </div>
    <PricingBoxOrder
      priceList={priceList}
      total={t("total")}
      btnDawnLoadPrice={t("downloadInvoice")}
    />
  </div>
);

OrderDetail.defaultProps = {
  orderNumber: "",
  submitted: "",
  scheduledToPublish: "",
  close: () => {},
  statusOrder: {},
  open: () => {},
  editPub: () => {},
  editPubDate: () => {},
  downloadJPG: () => {},
  downloadPDF: () => {},
  openStatus: {},
  priceList: [],
};

OrderDetail.propTypes = {
  t: PropTypes.func.isRequired,
  orderNumber: PropTypes.string,
  submitted: PropTypes.string,
  scheduledToPublish: PropTypes.string,
  close: PropTypes.func,
  downloadJPG: PropTypes.func,
  downloadPDF: PropTypes.func,
  statusOrder: PropTypes.shape({}),
  open: PropTypes.func,
  editPub: PropTypes.func,
  editPubDate: PropTypes.func,
  openStatus: PropTypes.shape({
    first: PropTypes.bool,
    second: PropTypes.bool,
  }),
  priceList: PropTypes.arrayOf(PropTypes.shape({})),
};

export default withTranslation("adOrderPage")(OrderDetail);
