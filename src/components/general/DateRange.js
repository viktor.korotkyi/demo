import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import styles from "../../../scss/components/general/DateRange.module.scss";
import Button from "../UI/Button";

const DateRange = ({ header, textFirst, textSecond, dateFirst, dateSecond, openModalDate }) => (
  <div className={styles.continer}>
    {dateFirst === "" && <span className={styles.header}>{header}</span>}
    <div className={styles.dateBox}>
      <div className={styles.picker}>
        {dateFirst !== "" && <span className={styles.header}>{textFirst}</span>}
        <Button color={dateFirst !== "" ? "activeBtnDate" : "buttonDate"} onClick={openModalDate}>
          {dateFirst === "" && (
            <div>
              <span>{textFirst}</span>
              <FontAwesomeIcon icon={faCalendar} />
            </div>
          )}
          {dateFirst !== "" && <span>{dateFirst}</span>}
        </Button>
      </div>
      {(dateFirst === "" || dateSecond !== "") && (
        <div className={styles.picker}>
          {dateSecond !== "" && <span className={styles.header}>{textSecond}</span>}
          <Button
            color={dateSecond !== "" ? "activeBtnDate" : "buttonDate"}
            onClick={openModalDate}
          >
            {dateSecond === "" && (
              <div>
                <span>{textSecond}</span>
                <FontAwesomeIcon icon={faCalendar} />
              </div>
            )}
            {dateSecond !== "" && <span>{dateSecond}</span>}
          </Button>
        </div>
      )}
    </div>
  </div>
);

DateRange.defaultProps = {
  header: "",
  textFirst: "",
  textSecond: "",
  dateFirst: "",
  dateSecond: "",
  openModalDate: () => {},
};

DateRange.propTypes = {
  header: PropTypes.string,
  textFirst: PropTypes.string,
  textSecond: PropTypes.string,
  dateFirst: PropTypes.string,
  dateSecond: PropTypes.string,
  openModalDate: PropTypes.func,
};

export default DateRange;
