import PropTypes from "prop-types";
import InputBox from "../UI/InputBox";
import TextAreaBox from "../UI/TextAreaBox";
import styles from "../../../scss/components/general/FormInputsPub.module.scss";

const FormInputsPub = ({ arrayInputs, handleTextPub }) => (
  <div className={styles.inputBox}>
    {arrayInputs.map((item, index) => {
      let render;
      if (item.type === "description") {
        render = (
          <TextAreaBox
            key={[index, item.id].join("_")}
            id={item.id}
            placeholder={item.text}
            value={item.text}
            onChange={handleTextPub}
            count
          />
        );
      } else if (item.type === "input") {
        render = (
          <InputBox
            key={[index, item.id].join("_")}
            id={item.id}
            value={item.text}
            maxlength={item.maxlength}
            autocomplete="off"
            onChange={handleTextPub}
            count
          />
        );
      }
      return render;
    })}
  </div>
);

FormInputsPub.defaultProps = {
  arrayInputs: [],
  settingsCrop: {},
  handleTextPub: () => {},
};

FormInputsPub.propTypes = {
  arrayInputs: PropTypes.arrayOf(PropTypes.shape({})),
  handleTextPub: PropTypes.func,
  settingsCrop: PropTypes.shape({}),
};

export default FormInputsPub;
