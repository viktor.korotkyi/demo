import PropTypes from "prop-types";
import Button from "../UI/Button";
import styles from "../../../scss/components/general/PanelPrevNext.module.scss";

const PanelPrevNext = ({
  previousOrders,
  btnPrev,
  count,
  totalCount,
  ofText,
  nextOrders,
  nextPrev,
  prev,
}) => (
  <div className={styles.container}>
    <div className={styles.prevDesktop}>
      <Button color="ordersMenu" onClick={previousOrders}>
        {btnPrev}
      </Button>
    </div>
    <div className={styles.prevMobile}>
      <Button color="ordersMenu" onClick={previousOrders}>
        {prev}
      </Button>
    </div>
    <div className={styles.count}>
      <span>{`${count.first}`}</span>
      <span>-</span>
      <span>{`${count.last}`}</span>
      <span>{ofText}</span>
      <span>{`${totalCount}`}</span>
    </div>
    <Button color="ordersMenu" onClick={nextOrders}>
      {nextPrev}
    </Button>
  </div>
);

PanelPrevNext.defaultProps = {
  previousOrders: () => {},
  nextOrders: () => {},
  btnPrev: "",
  nextPrev: "",
  ofText: "",
  prev: "",
  count: {},
  totalCount: 0,
};

PanelPrevNext.propTypes = {
  previousOrders: PropTypes.func,
  nextOrders: PropTypes.func,
  btnPrev: PropTypes.string,
  nextPrev: PropTypes.string,
  ofText: PropTypes.string,
  prev: PropTypes.string,
  count: PropTypes.shape({
    first: PropTypes.number,
    last: PropTypes.number,
  }),
  totalCount: PropTypes.number,
};

export default PanelPrevNext;
