import PropTypes from "prop-types";

import styles from "../../../scss/components/general/CardTemplate.module.scss";

const CardTemplate = ({ src, selectOneText, chooseTemplate, id, active, openModal }) => (
  <div className={styles.container}>
    <div
      onClick={() => chooseTemplate(id)}
      className={`${styles.templateView} ${active && styles.active}`}
      onKeyUp={() => chooseTemplate(id)}
      role="button"
      tabIndex="0"
    >
      <img id={id} src={src} alt="template" />
    </div>
    {/* <div
      className={styles.selectOne}
      onClick={() => chooseTemplate(id)}
      onKeyUp={() => chooseTemplate(id)}
      role="button"
      tabIndex="0"
    >
      <span>{selectOneText}</span>
    </div> */}
  </div>
);

CardTemplate.defaultProps = {
  selectOneText: "",
  src: "",
  id: "",
  active: false,
  chooseTemplate: () => {},
  openModal: () => {},
};

CardTemplate.propTypes = {
  selectOneText: PropTypes.string,
  src: PropTypes.string,
  id: PropTypes.string,
  active: PropTypes.bool,
  chooseTemplate: PropTypes.func,
  openModal: PropTypes.func,
};

export default CardTemplate;
