import PropTypes from "prop-types";
import InputBox from "../UI/InputBox";
import styles from "../../../scss/components/general/LineAddress.module.scss";

const LineAddress = ({
  form,
  handleForm,
  placeholderAddress,
  placeholderSuit,
  labelFirst,
  labelSecond,
}) => (
  <div className={styles.container}>
    <InputBox
      id="address"
      onChange={handleForm}
      label={labelFirst}
      placeholder={placeholderAddress}
      name="address"
      value={form.address}
      autocomplete="off"
      textStyle="textNormal"
    />
    <InputBox
      id="suite"
      onChange={handleForm}
      label={labelSecond}
      placeholder={placeholderSuit}
      name="suite"
      value={form.suite}
      autocomplete="off"
      textStyle="textNormal"
    />
  </div>
);

LineAddress.defaultProps = {
  handleForm: () => {},
  form: {},
  placeholderAddress: "",
  placeholderSuit: "",
  labelFirst: "",
  labelSecond: "",
};

LineAddress.propTypes = {
  handleForm: PropTypes.func,
  form: PropTypes.shape({
    address: PropTypes.string,
    suite: PropTypes.string,
  }),
  placeholderAddress: PropTypes.string,
  placeholderSuit: PropTypes.string,
  labelFirst: PropTypes.string,
  labelSecond: PropTypes.string,
};

export default LineAddress;
