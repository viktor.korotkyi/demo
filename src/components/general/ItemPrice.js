import PropTypes from "prop-types";
import styles from "../../../scss/components/general/ItemPrice.module.scss";

const ItemPrice = ({ item, price, styleType }) => (
  <div className={`${styles.container} ${styleType && styles[styleType]}`}>
    <span>{item}</span>
    <span>{`$${price}`}</span>
  </div>
);

ItemPrice.defaultProps = {
  item: "",
  price: "",
  styleType: "",
};

ItemPrice.propTypes = {
  item: PropTypes.string,
  price: PropTypes.string,
  styleType: PropTypes.string,
};

export default ItemPrice;
