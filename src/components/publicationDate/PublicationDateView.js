import PropTypes from "prop-types";
import styles from "../../../scss/components/publicationDate/PublicationDateView.module.scss";
import Wrapper from "../../layouts/Wrapper";
import HeaderBox from "../general/HeaderBox";
import CalendarPublication from "./CalendarPublication";
import DateFilter from "./DateFilter";

const PublicationDateView = ({
  header,
  text,
  titleButton,
  titleButtonSecond,
  headerFilter,
  handleFilter,
  activeFilter,
  actualDate,
  previousMonth,
  nextMonth,
  amountOfDays,
  handleChooseDay,
  dayChoose,
  infoMonth,
  unavailableDays,
}) => (
  <div className={styles.container}>
    <Wrapper styleWrapper="wrapperDatePublication">
      <HeaderBox header={header} text={text} />
      <DateFilter
        titleButton={titleButton}
        titleButtonSecond={titleButtonSecond}
        headerFilter={headerFilter}
        handleFilter={handleFilter}
        activeFilter={activeFilter}
      />
      <CalendarPublication
        month={actualDate.month}
        prevBtnMonth="previous"
        nextBtnMonth="next"
        previousMonth={previousMonth}
        nextMonth={nextMonth}
        amountOfDays={amountOfDays}
        handleChooseDay={handleChooseDay}
        dayChoose={dayChoose}
        infoMonth={infoMonth}
        unavailableDays={unavailableDays}
      />
    </Wrapper>
  </div>
);

PublicationDateView.defaultProps = {
  header: "",
  text: "",
  titleButton: "",
  titleButtonSecond: "",
  headerFilter: "",
  activeFilter: false,
  actualDate: {},
  handleFilter: () => {},
  previousMonth: () => {},
  nextMonth: () => {},
  amountOfDays: 0,
  handleChooseDay: () => {},
  dayChoose: {},
  infoMonth: {},
  unavailableDays: [],
};

PublicationDateView.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  titleButton: PropTypes.string,
  titleButtonSecond: PropTypes.string,
  headerFilter: PropTypes.string,
  activeFilter: PropTypes.bool,
  actualDate: PropTypes.shape({
    month: PropTypes.string,
  }),
  handleFilter: PropTypes.func,
  previousMonth: PropTypes.func,
  nextMonth: PropTypes.func,
  amountOfDays: PropTypes.number,
  handleChooseDay: PropTypes.func,
  dayChoose: PropTypes.shape({}),
  infoMonth: PropTypes.shape({}),
  unavailableDays: PropTypes.arrayOf(PropTypes.string),
};

export default PublicationDateView;
