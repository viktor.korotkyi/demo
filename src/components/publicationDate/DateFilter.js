import PropTypes from "prop-types";
import Button from "../UI/Button";
import styles from "../../../scss/components/publicationDate/DateFilter.module.scss";

const DateFilter = ({
  titleButton,
  titleButtonSecond,
  headerFilter,
  handleFilter,
  activeFilter,
}) => (
  <div className={styles.container}>
    <span>{headerFilter}</span>
    <Button
      color="btnDateFilter"
      id="allPub"
      onClick={handleFilter}
      active={activeFilter && "btnDateFilterActive"}
    >
      {titleButton}
    </Button>
    <Button
      color="btnDateFilter"
      id="oneByOne"
      onClick={handleFilter}
      active={!activeFilter && "btnDateFilterActive"}
    >
      {titleButtonSecond}
    </Button>
  </div>
);

DateFilter.defaultProps = {
  titleButton: "",
  titleButtonSecond: "",
  headerFilter: "",
  activeFilter: false,
  handleFilter: () => {},
};

DateFilter.propTypes = {
  titleButton: PropTypes.string,
  titleButtonSecond: PropTypes.string,
  headerFilter: PropTypes.string,
  activeFilter: PropTypes.bool,
  handleFilter: PropTypes.func,
};

export default DateFilter;
