import PropTypes from "prop-types";
import styles from "../../../scss/components/publicationDate/CalendarPublication.module.scss";
import {
  checkAvailableDay,
  createArray,
  getWeekDay,
  getMonthTitle,
} from "../../utils/storageWorks";
import Button from "../UI/Button";

const CalendarPublication = ({
  prevBtnMonth,
  nextBtnMonth,
  previousMonth,
  nextMonth,
  amountOfDays,
  handleChooseDay,
  dayChoose,
  infoMonth,
  unavailableDays,
}) => (
  <div className={styles.containerMain}>
    <span className={styles.headerMain}>
      {`${getMonthTitle(infoMonth.year, infoMonth.month)} ${infoMonth.year}`}
    </span>
    <div className={styles.bodyCalendar}>
      {createArray(amountOfDays).map((item, index) => (
        <Button
          key={[item, index].join("_")}
          color={
            checkAvailableDay(unavailableDays, infoMonth.year, infoMonth.month, `${index + 1}`)
              ? "buttonDayChooseUnavailable"
              : "buttonDayChoose"
          }
          active={dayChoose[`${index + 1}`.padStart(2, "0")] && "buttonDayChooseActive"}
          onClick={handleChooseDay}
          id={`${index + 1}`.padStart(2, "0")}
        >
          <span id={`${index + 1}`.padStart(2, "0")}>{`${index + 1}`.padStart(2, "0")}</span>
          <span id={`${index + 1}`.padStart(2, "0")}>
            {getWeekDay(infoMonth.year, infoMonth.month, `${index + 1}`.padStart(2, "0"))}
          </span>
          <span id={`${index + 1}`.padStart(2, "0")}>120$</span>
        </Button>
      ))}
    </div>
    <div className={styles.navigationBar}>
      <Button onClick={previousMonth} color="btnCalendarNavigation">
        <span>{prevBtnMonth}</span>
      </Button>
      <span className={styles.navMonth}>{getMonthTitle(infoMonth.year, infoMonth.month)}</span>
      <Button onClick={nextMonth} color="btnCalendarNavigation">
        <span>{nextBtnMonth}</span>
      </Button>
    </div>
  </div>
);

CalendarPublication.defaultProps = {
  month: "",
  prevBtnMonth: "",
  nextBtnMonth: "",
  previousMonth: () => {},
  nextMonth: () => {},
  amountOfDays: 0,
  handleChooseDay: () => {},
  dayChoose: {},
  infoMonth: {},
  unavailableDays: [],
};

CalendarPublication.propTypes = {
  month: PropTypes.string,
  prevBtnMonth: PropTypes.string,
  nextBtnMonth: PropTypes.string,
  previousMonth: PropTypes.func,
  nextMonth: PropTypes.func,
  amountOfDays: PropTypes.number,
  handleChooseDay: PropTypes.func,
  dayChoose: PropTypes.shape({}),
  infoMonth: PropTypes.shape({
    year: PropTypes.number,
    month: PropTypes.number,
  }),
  unavailableDays: PropTypes.arrayOf(PropTypes.string),
};

export default CalendarPublication;
