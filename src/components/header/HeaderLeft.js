import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Logo from "../general/Logo";
import LinkRef from "../general/LinkRef";
import styles from "../../../scss/components/header/HeaderLeft.module.scss";
import TextList from "../general/TextList";
import FrameBanner from "../general/FrameBanner";

const HeaderLeft = ({ t, asPath, selectedItem }) => (
  <div className={styles.container}>
    <Logo href="/" urlLogo="/static/images/logo.png" styleType="logoLeftHeader" />
    {asPath === "/create-account" && (
      <div className={styles.loginBox}>
        <span className={styles.label}>{t("alreadyHaveAnAccount")}</span>
        <LinkRef href="/login" typeStyle="createAccount">
          <span>{t("logInToYourAccount")}</span>
        </LinkRef>
      </div>
    )}

    {(asPath === "/create-publication" ||
      asPath === "/choose-package" ||
      asPath === "/choose-template" ||
      asPath === "/publication-date") && (
      <div className={styles.boxInfo}>
        <TextList title={t("yourSelectedClassified")} selectedItem={selectedItem} />
        <FrameBanner text={t("bannerSpace")} size={t("sizeBanner")} />
      </div>
    )}

    <div className={styles.logoFooter}>
      <Logo href="/" styleType="logoFooter" urlLogo="/static/images/logoFooter/branding.png" />
      <div className={styles.policeBox}>
        <LinkRef href="/privacy" typeStyle="policyLink">
          {t("privacy")}
        </LinkRef>
        <LinkRef href="/privacy" typeStyle="policyLink">
          {t("terms")}
        </LinkRef>
      </div>
    </div>
  </div>
);

HeaderLeft.defaultProps = {
  asPath: "",
  selectedItem: "",
};

HeaderLeft.propTypes = {
  t: PropTypes.func.isRequired,
  asPath: PropTypes.string,
  selectedItem: PropTypes.shape({}),
};

export default withTranslation("indexPage")(HeaderLeft);
