import PropTypes from "prop-types";
import styles from "../../../scss/components/upgradeSocial/UpgradeSocialView.module.scss";
import FormBuildPub from "../general/FormBuildPub";
import LivePreview from "../general/LivePreview";
import HeaderUpgradeView from "../general/HeaderUpgradeView";

const UpgradeSocialView = ({
  titleUpload,
  titleRightUpload,
  uploadFile,
  fileUpload,
  textPub,
  handleTextPub,
  labelInput,
  labelInputSub,
  labelBody,
  template,
  titleLiveView,
  title,
  underTitle,
  text,
  placeholderSelect,
  titleSelect,
  listTargetDemographic,
  handleSelect,
  arrayInputs,
  deleteUploadFile,
  livePreview,
  chooseDate,
  handleCroppedImage,
  settingsCrop,
  onCropChange,
  onCropComplete,
  onZoomChange,
  onRotationChange,
  rotation,
  rotateleft,
  rotateRight,
}) => (
  <div className={styles.container}>
    <div className={styles.formBox}>
      <HeaderUpgradeView title={title} underTitle={underTitle} text={text} />
      <div className={styles.paddingBox}>
        <FormBuildPub
          titleRightUpload={titleRightUpload}
          titleUpload={titleUpload}
          uploadFile={uploadFile}
          fileUpload={fileUpload}
          textPub={textPub}
          handleTextPub={handleTextPub}
          labelInput={labelInput}
          labelInputSub={labelInputSub}
          labelBody={labelBody}
          placeholderSelect={placeholderSelect}
          titleSelect={titleSelect}
          listSelect={listTargetDemographic}
          handleSelect={handleSelect}
          arrayInputs={arrayInputs}
          deleteUploadFile={deleteUploadFile}
          hideDateRange={false}
          hideChooseTarget
          handleCroppedImage={handleCroppedImage}
          settingsCrop={settingsCrop}
          onCropChange={onCropChange}
          onCropComplete={onCropComplete}
          onZoomChange={onZoomChange}
          onRotationChange={onRotationChange}
          rotation={rotation}
          rotateleft={rotateleft}
          rotateRight={rotateRight}
        />
      </div>
    </div>
    <div className={styles.viewBox}>
      <LivePreview
        titleLiveView={titleLiveView}
        textPub={textPub}
        template={template}
        livePreview={livePreview}
        chooseDate={chooseDate}
      />
    </div>
  </div>
);

UpgradeSocialView.defaultProps = {
  titleUpload: "",
  titleRightUpload: "",
  fileUpload: "",
  textPub: {},
  labelInput: "",
  labelInputSub: "",
  labelBody: "",
  titleLiveView: "",
  title: "",
  underTitle: "",
  text: "",
  placeholderSelect: "",
  titleSelect: "",
  listTargetDemographic: [],
  chooseDate: {},
  template: {},
  arrayInputs: [],
  handleTextPub: () => {},
  uploadFile: () => {},
  handleSelect: () => {},
  deleteUploadFile: () => {},
  livePreview: false,
  handleCroppedImage: () => {},
  settingsCrop: {},
  onCropChange: () => {},
  onCropComplete: () => {},
  onZoomChange: () => {},
  onRotationChange: () => {},
  rotation: "",
  rotateleft: () => {},
  rotateRight: () => {},
};

UpgradeSocialView.propTypes = {
  titleUpload: PropTypes.string,
  titleRightUpload: PropTypes.string,
  fileUpload: PropTypes.string,
  labelInput: PropTypes.string,
  labelInputSub: PropTypes.string,
  labelBody: PropTypes.string,
  titleLiveView: PropTypes.string,
  title: PropTypes.string,
  underTitle: PropTypes.string,
  text: PropTypes.string,
  placeholderSelect: PropTypes.string,
  titleSelect: PropTypes.string,
  textPub: PropTypes.shape({}),
  chooseDate: PropTypes.shape({}),
  template: PropTypes.shape({}),
  handleTextPub: PropTypes.func,
  uploadFile: PropTypes.func,
  handleSelect: PropTypes.func,
  deleteUploadFile: PropTypes.func,
  arrayInputs: PropTypes.arrayOf(PropTypes.shape({})),
  listTargetDemographic: PropTypes.arrayOf(PropTypes.shape({})),
  livePreview: PropTypes.bool,
  handleCroppedImage: PropTypes.func,
  settingsCrop: PropTypes.shape({}),
  onCropChange: PropTypes.func,
  onCropComplete: PropTypes.func,
  onZoomChange: PropTypes.func,
  onRotationChange: PropTypes.func,
  rotation: PropTypes.string,
  rotateleft: PropTypes.func,
  rotateRight: PropTypes.func,
};

export default UpgradeSocialView;
