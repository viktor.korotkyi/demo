import PropTypes from "prop-types";
import Button from "../UI/Button";
import styles from "../../../scss/components/infoPage/PageHeader.module.scss";

const PageHeader = ({ header, btnName, onClick }) => (
  <div className={styles.container}>
    <h3>{header}</h3>
    <Button color="buttonInfoHeader" onClick={onClick}>
      {btnName}
    </Button>
  </div>
);

PageHeader.defaultProps = {
  header: "",
  btnName: "",
  onClick: () => {},
};

PageHeader.propTypes = {
  header: PropTypes.string,
  btnName: PropTypes.string,
  onClick: PropTypes.func,
};

export default PageHeader;
