import PropTypes from "prop-types";
import TextBox from "../UI/TextBox";
import styles from ",,/../../scss/components/infoPage/InfoContent.module.scss";

const InfoContent = ({ data, styleType }) => (
  <div className={`${styles.container}`}>
    {data &&
      data.map((item, index) => (
        <TextBox
          key={[index, item.name].join("_")}
          header={item.name !== "" ? item.name : Object.values(item.week).map((elem) => elem)}
          text={item.text}
          textInformation={item.textInformation}
          styleType={styleType}
        />
      ))}
  </div>
);

InfoContent.defaultProps = {
  data: [],
  styleType: "",
};

InfoContent.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
  styleType: PropTypes.string,
};

export default InfoContent;
