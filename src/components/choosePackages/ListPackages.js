import PropTypes from "prop-types";
import styles from "../../../scss/components/choosePackages/ListPackages.module.scss";
import Wrapper from "../../layouts/Wrapper";
import CardPackage from "../general/CardPackage";
import HeaderBox from "../general/HeaderBox";

const ListPackages = ({ header, text, dataPackages, handleChoosePackage, packageList }) => (
  <div className={styles.listContainer}>
    <Wrapper styleWrapper="wrapperCreatePublication" styleContainer="containerHightAuto">
      <HeaderBox header={header} text={text} />
      <div className={styles.contentContainer}>
        {dataPackages &&
          dataPackages.map((item, index) => (
            <CardPackage
              key={[item.name, index].join("_")}
              id={item.id}
              info={item.info}
              name={item.name}
              price={item.price}
              clickCard={handleChoosePackage}
              active={packageList[item.id]}
            />
          ))}
      </div>
    </Wrapper>
  </div>
);

ListPackages.defaultProps = {
  header: "",
  text: "",
  dataPackages: [],
  handleChoosePackage: () => {},
  packageList: {},
};

ListPackages.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  dataPackages: PropTypes.arrayOf(PropTypes.shape({})),
  handleChoosePackage: PropTypes.func,
  packageList: PropTypes.shape({}),
};

export default ListPackages;
