import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import InputBox from "../UI/InputBox";
import Button from "../UI/Button";
import LineAddress from "../general/LineAddress";
import LineCity from "../general/LinyCity";
import SelectManyApply from "./SelectManyApply";
import styles from "../../../scss/components/createAccount/CreateAccountForm.module.scss";

const CreateAccountForm = ({
  t,
  submitSaveContinue,
  form,
  handleForm,
  state,
  city,
  formApply,
  handleFormApply,
}) => (
  <div className={styles.container}>
    <h3>{t("createAccountHeader")}</h3>
    <form className={styles.form} onSubmit={submitSaveContinue}>
      <InputBox
        label={t("emailAddress")}
        placeholder={t("emailAddress")}
        onChange={handleForm}
        name="email"
        value={form.email}
        type="email"
      />
      <div className={styles.password}>
        <InputBox
          label={t("password")}
          placeholder={t("password")}
          onChange={handleForm}
          name="password"
          value={form.password}
        />
      </div>
      <InputBox
        onChange={handleForm}
        label={t("phoneNumber")}
        placeholder={t("phoneNumber")}
        name="phone"
        value={form.phone}
        type="number"
        autocomplete="off"
        textStyle="marginTextCreate"
      />
      <LineAddress
        form={form}
        handleForm={handleForm}
        placeholderAddress={t("addressLine1")}
        placeholderSuit={t("suiteUnit")}
      />
      <LineCity
        form={form}
        handleForm={handleForm}
        state={state}
        city={city}
        placeholderCity={t("city")}
        placeholderState={t("state")}
        chooseLabel={t("chooseOption")}
      />
      <InputBox
        onChange={handleForm}
        placeholder={t("zipCode")}
        name="zipCode"
        value={form.zipCode}
        type="number"
        autocomplete="off"
      />
      <SelectManyApply
        selectText={t("selected")}
        deselectText={t("deselected")}
        formApply={formApply}
        handleFormApply={handleFormApply}
        label={t("selectApply")}
      />
      <Button
        color="buttonSaveContinue"
        typeBtn={false}
        disabled={form.email === "" || form.phone === ""}
      >
        {t("saveContinue")}
      </Button>
    </form>
  </div>
);

CreateAccountForm.defaultProps = {
  submitSaveContinue: () => {},
  handleForm: () => {},
  handleFormApply: () => {},
  form: {},
  formApply: {},
  state: [],
  city: [],
};

CreateAccountForm.propTypes = {
  t: PropTypes.func.isRequired,
  submitSaveContinue: PropTypes.func,
  handleForm: PropTypes.func,
  form: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string,
    phone: PropTypes.string,
    zipCode: PropTypes.string,
  }),
  state: PropTypes.arrayOf(PropTypes.string),
  city: PropTypes.arrayOf(PropTypes.string),
  formApply: PropTypes.shape({}),
  handleFormApply: PropTypes.func,
};

export default withTranslation("createAccount")(CreateAccountForm);
