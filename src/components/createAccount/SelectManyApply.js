import PropTypes from "prop-types";
import CheckButton from "../UI/CheckButton";

import styles from "../../../scss/components/createAccount/SelectManyApply.module.scss";

const SelectManyApply = ({ label, formApply, handleFormApply, selectText, deselectText }) => (
  <div className={styles.container}>
    <span>{label}</span>
    {formApply &&
      Object.keys(formApply).map((item, index) => (
        <CheckButton
          key={[index, item].join("_")}
          onClick={handleFormApply}
          text={formApply[item] ? selectText : deselectText}
          active={formApply[item]}
          id={`${item}`}
        />
      ))}
  </div>
);

SelectManyApply.defaultProps = {
  label: "",
  selectText: "",
  deselectText: "",
  formApply: {},
  handleFormApply: () => {},
};

SelectManyApply.propTypes = {
  label: PropTypes.string,
  selectText: PropTypes.string,
  deselectText: PropTypes.string,
  formApply: PropTypes.shape({}),
  handleFormApply: PropTypes.func,
};

export default SelectManyApply;
