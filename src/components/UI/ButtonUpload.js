import PropTypes from "prop-types";
import styles from "../../../scss/components/general/ButtonUpload.module.scss";

const ButtonUpload = ({ onClick, id, image }) => (
  <div>
    <label htmlFor={id} className={styles.container}>
      {image ? (
        <img src={image} alt="dummy" width="300" height="300" />
      ) : (
        <img src="/static/images/upload-image.png" alt="upload" />
      )}
    </label>
    <input id={id} type="file" style={{ display: "none" }} onChange={onClick} />
  </div>
);

ButtonUpload.defaultProps = {
  onClick: () => {},
  image: "",
  id: "",
};

ButtonUpload.propTypes = {
  onClick: PropTypes.func,
  image: PropTypes.string,
  id: PropTypes.string,
};

export default ButtonUpload;
