import PropTypes from "prop-types";
import { useState } from "react";

import styles from "../../../scss/components/general/Select.module.scss";

const Select = ({ label, name, onChange, list, value, placeholder, chooseLabel, allWidth }) => {
  const [openSelect, setOpenSelect] = useState(false);
  const placeholderSelect = openSelect ? chooseLabel : placeholder;
  const styleBox = openSelect ? "open" : "close";
  const height = list.length > 5 && openSelect ? "boxListOpen" : "";
  const openListBoxAllWidth = openSelect && allWidth ? "openListBoxAllWidth" : "";
  const colorText = value !== "" ? "selected" : "";
  const openWithLabel = label && "openWithLabel";

  const handleSelect = () => {
    setOpenSelect(!openSelect);
  };

  return (
    <div className={`${styles.container} ${styles[openListBoxAllWidth]}`}>
      <span>{label}</span>
      <div
        className={`${styles.select} ${styles[styleBox]} ${styles[openWithLabel]}`}
        onClick={handleSelect}
        onMouseLeave={() => setOpenSelect(false)}
      >
        <span className={styles[colorText]}>{value || placeholderSelect}</span>
        <div className={`${styles.boxList} ${styles[height]}`}>
          {openSelect &&
            list.map((item, index) => (
              <span
                key={[item, index].join("_")}
                className={styles.option}
                id={name}
                onClick={onChange}
                value={item}
              >
                {item}
              </span>
            ))}
        </div>
      </div>
    </div>
  );
};
Select.defaultProps = {
  name: "",
  label: "",
  value: "",
  placeholder: "",
  chooseLabel: "",
  onChange: () => {},
  list: [],
  allWidth: false,
};

Select.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  chooseLabel: PropTypes.string,
  onChange: PropTypes.func,
  list: PropTypes.arrayOf(PropTypes.string),
  allWidth: PropTypes.bool,
};

export default Select;
