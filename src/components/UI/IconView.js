import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import styles from "../../../scss/components/general/IconView.module.scss";

const IconView = ({ listView, handleViewList, icon, id, styleType }) => (
  <div className={`${styles.icon} ${listView && styles.active} ${styleType && styles[styleType]}`}>
    <FontAwesomeIcon icon={icon} id={id} onClick={handleViewList} />
  </div>
);

IconView.defaultProps = {
  listView: true,
  icon: {},
  id: "",
  styleType: "",
  handleViewList: () => {},
};

IconView.propTypes = {
  listView: PropTypes.bool,
  handleViewList: PropTypes.func,
  icon: PropTypes.shape({}),
  id: PropTypes.string,
  styleType: PropTypes.string,
};

export default IconView;
