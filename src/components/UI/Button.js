import PropTypes from "prop-types";

import styles from "../../../scss/components/general/Button.module.scss";

const Button = ({ children, onClick, color, typeBtn, disabled, name, id, active }) => (
  <button
    className={`${styles.btn} ${color && styles[color]} ${active && styles[active]}`}
    onClick={(e) => onClick(e, id)}
    type={typeBtn ? "button" : "submit"}
    disabled={disabled}
    name={name}
    id={id}
  >
    {children}
  </button>
);

Button.defaultProps = {
  onClick: () => {},
  color: "",
  typeBtn: true,
  disabled: false,
  name: "",
  id: "",
  active: false,
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  color: PropTypes.string,
  typeBtn: PropTypes.bool,
  disabled: PropTypes.bool,
  name: PropTypes.string,
  id: PropTypes.string,
  active: PropTypes.any,
};

export default Button;
