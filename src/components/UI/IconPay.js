import PropTypes from "prop-types";
import styles from "../../../scss/components/general/IconPay.module.scss";
import IconView from "./IconView";

const IconPay = ({ faIcon, card, handleActivePayType, id, activePayType }) => (
  <div
    className={`${styles.typePay}
     ${(card === "Card" ? !activePayType : activePayType) && styles.activePay}`}
    onClick={handleActivePayType}
  >
    <IconView icon={faIcon} id={id} styleType="pay" />
    <span>{card}</span>
  </div>
);

IconPay.defaultProps = {
  id: "",
  card: "",
  activePayType: false,
  handleActivePayType: () => {},
};

IconPay.propTypes = {
  faIcon: PropTypes.any.isRequired,
  card: PropTypes.string,
  id: PropTypes.string,
  activePayType: PropTypes.bool,
  handleActivePayType: PropTypes.func,
};

export default IconPay;
