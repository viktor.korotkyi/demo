import PropTypes from "prop-types";

import styles from "../../../scss/components/general/TextBox..module.scss";

const TextBox = ({ header, text, textInformation, styleType }) => (
  <div className={`${styles.container} ${styleType && styles[styleType]}`}>
    <span>{header}</span>
    <span>{text}</span>
    <span>{textInformation}</span>
  </div>
);

TextBox.defaultProps = {
  header: "",
  text: "",
  textInformation: "",
  styleType: "",
};

TextBox.propTypes = {
  header: PropTypes.node,
  text: PropTypes.string,
  styleType: PropTypes.string,
  textInformation: PropTypes.string,
};

export default TextBox;
