import PropTypes from "prop-types";
import TextArea from "./TextArea";
import LabelBox from "../general/LabelBox";
import styles from "../../../scss/components/general/TextAreaBox.module.scss";

const TextAreaBox = ({
  onChange,
  placeholder,
  name,
  value,
  disable,
  styleType,
  label,
  textStyle,
  id,
  count,
}) => (
  <div className={`${styles.container} ${textStyle && styles[textStyle]}`}>
    <LabelBox count={count} label={label} value={value} />
    <TextArea
      id={id}
      onChange={onChange}
      placeholder={placeholder}
      name={name}
      value={value}
      disable={disable}
      styleType={styleType}
    />
  </div>
);

TextAreaBox.defaultProps = {
  placeholder: "",
  name: "",
  value: "",
  disable: "",
  styleType: "",
  textStyle: "",
  label: "",
  id: "",
  count: false,
  onChange: () => {},
};

TextAreaBox.propTypes = {
  placeholder: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.node,
  onChange: PropTypes.func,
  disable: PropTypes.string,
  styleType: PropTypes.string,
  textStyle: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string,
  count: PropTypes.bool,
};

export default TextAreaBox;
