import PropTypes from "prop-types";
import Button from "./Button";
import HeaderActiveModal from "../general/HeaderActiveModal";

const ButtonCount = ({
  handleStatusModal,
  statusModal,
  count,
  enterZipCode,
  stylesType,
  totalCount,
}) => (
  <Button onClick={handleStatusModal} color="btnModalZip">
    <HeaderActiveModal
      count={count}
      enterZipCode={enterZipCode}
      statusModal={statusModal}
      stylesType={stylesType}
      totalCount={totalCount}
    />
  </Button>
);

ButtonCount.defaultProps = {
  enterZipCode: "",
  count: "",
  totalCount: "",
  stylesType: "",
  statusModal: false,
  handleStatusModal: () => {},
};

ButtonCount.propTypes = {
  enterZipCode: PropTypes.string,
  count: PropTypes.string,
  totalCount: PropTypes.string,
  stylesType: PropTypes.string,
  statusModal: PropTypes.bool,
  handleStatusModal: PropTypes.func,
};

export default ButtonCount;
