import PropTypes from "prop-types";
import Input from "./Input";
import LabelBox from "../general/LabelBox";
import styles from "../../../scss/components/general/InputBox.module.scss";

const InputBox = ({
  onChange,
  placeholder,
  name,
  value,
  type,
  disable,
  maxlength,
  styleType,
  label,
  textStyle,
  autocomplete,
  id,
  count,
  autofocus,
  onBlur,
}) => (
  <div className={`${styles.container} ${textStyle && styles[textStyle]}`}>
    {label && <LabelBox count={count} label={label} maxlength={maxlength} value={value} />}
    <Input
      id={id}
      onChange={onChange}
      placeholder={placeholder}
      name={name}
      value={value}
      type={type}
      disable={disable}
      maxlength={maxlength}
      styleType={styleType}
      autocomplete={autocomplete}
      autofocus={autofocus}
      onBlur={onBlur}
    />
  </div>
);

InputBox.defaultProps = {
  placeholder: "",
  name: "",
  value: "",
  type: "text",
  disable: "",
  maxlength: "256",
  styleType: "",
  textStyle: "",
  label: "",
  autocomplete: "on",
  id: "",
  count: false,
  autofocus: false,
  onChange: () => {},
  onBlur: () => {},
};

InputBox.propTypes = {
  placeholder: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.node,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  disable: PropTypes.string,
  maxlength: PropTypes.string,
  styleType: PropTypes.string,
  textStyle: PropTypes.string,
  label: PropTypes.string,
  autocomplete: PropTypes.string,
  id: PropTypes.string,
  count: PropTypes.bool,
  autofocus: PropTypes.bool,
};

export default InputBox;
