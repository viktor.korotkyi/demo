import PropTypes from "prop-types";

import styles from "../../../scss/components/general/TextArea.module.scss";

const TextArea = ({
  onChange,
  placeholder,
  name,
  value,
  disable,
  styleType,
  requiredInput,
  id,
}) => (
  <textarea
    className={`${styles.textArea} ${styleType && styles[styleType]}`}
    placeholder={placeholder}
    name={name}
    onChange={onChange}
    value={value}
    disabled={disable}
    required={requiredInput}
    id={id}
  />
);

TextArea.defaultProps = {
  placeholder: "",
  name: "",
  value: "",
  disable: "",
  styleType: "",
  requiredInput: false,
  id: "",
  onChange: () => {},
};

TextArea.propTypes = {
  placeholder: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.node,
  onChange: PropTypes.func,
  disable: PropTypes.string,
  styleType: PropTypes.string,
  requiredInput: PropTypes.bool,
  id: PropTypes.string,
};

export default TextArea;
