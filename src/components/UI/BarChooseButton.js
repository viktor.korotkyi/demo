import PropTypes from "prop-types";
import CheckButton from "./CheckButton";

import styles from "../../../scss/components/general/BarChooseButton.module.scss";

const BarChooseButton = ({
  listButton,
  title,
  active,
  clickChooseClassifield,
  idBlock,
  listView,
}) => (
  <div className={`${styles.container} ${!listView && styles.gallery}`}>
    <div className={styles.header}>
      <h3>{title}</h3>
    </div>
    <div className={`${styles.boxBtn} ${!listView && styles.scroll}`}>
      <div>
        {listButton.map((item, index) => (
          <CheckButton
            key={[item, index].join("_")}
            active={active[item]}
            id={item}
            onClick={() => clickChooseClassifield(idBlock, item)}
            text={item}
          />
        ))}
      </div>
    </div>
  </div>
);

BarChooseButton.defaultProps = {
  listButton: [],
  title: "",
  idBlock: "",
  active: {},
  clickChooseClassifield: () => {},
  listView: true,
};

BarChooseButton.propTypes = {
  listButton: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string,
  idBlock: PropTypes.string,
  active: PropTypes.shape({}),
  clickChooseClassifield: PropTypes.func,
  listView: PropTypes.bool,
};

export default BarChooseButton;
