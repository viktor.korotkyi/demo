import PropTypes from "prop-types";
import styles from "../../../scss/components/general/Arrow.module.scss";

const Arrow = ({ type, styleBtn }) => (
  <div className={`${styles.btn} ${styleBtn && styles[styleBtn]}`}>
    <div className={`${styles.arrow} ${type && styles[type]}`} />
  </div>
);

Arrow.defaultProps = {
  type: "",
  styleBtn: "",
};

Arrow.propTypes = {
  type: PropTypes.string,
  styleBtn: PropTypes.string,
};

export default Arrow;
