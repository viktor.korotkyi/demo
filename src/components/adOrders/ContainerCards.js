import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Wrapper from "../../layouts/Wrapper";
import CardPublication from "../general/CardPublication";

const ContainerCards = ({ t, adType, cardsPub }) => (
  <Wrapper styleWrapper="wrapperContainerCards">
    {cardsPub &&
      cardsPub.map((item, index) => (
        <CardPublication
          key={[item, index].join("_")}
          imgPath="/static/images/card-photo.png"
          order={t("order")}
          orderNumber="#12345678"
          orderPlaced={t("orderPlaced")}
          textBtn={t("adType")}
          adType={adType}
        />
      ))}
  </Wrapper>
);

ContainerCards.defaultProps = {
  adType: () => {},
  cardsPub: [],
};

ContainerCards.propTypes = {
  t: PropTypes.func.isRequired,
  adType: PropTypes.func,
  cardsPub: PropTypes.arrayOf(PropTypes.shape({})),
};

export default withTranslation("adOrderPage")(ContainerCards);
