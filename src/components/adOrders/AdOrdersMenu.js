import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import AdOrdersListMenu from "./AdOrdersListMenu";
import Button from "../UI/Button";

import styles from "../../../scss/components/adOrders/AdOrdersMenu.module.scss";

const AdOrdersMenu = ({ t, handleMenuActivity, menuActive }) => (
  <div className={styles.container}>
    {AdOrdersListMenu.map((item, index) => (
      <Button
        color={menuActive[item] ? "ordersMenuActivity" : "ordersMenu"}
        onClick={handleMenuActivity}
        id={item}
        key={[item, index].join("_")}
      >
        {t(`${item}`)}
      </Button>
    ))}
  </div>
);

AdOrdersMenu.defaultProps = {
  handleMenuActivity: () => {},
  menuActive: {},
};

AdOrdersMenu.propTypes = {
  t: PropTypes.func.isRequired,
  handleMenuActivity: PropTypes.func,
  menuActive: PropTypes.shape({}),
};

export default withTranslation("adOrderPage")(AdOrdersMenu);
