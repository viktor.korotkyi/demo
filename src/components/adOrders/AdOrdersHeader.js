import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Button from "../UI/Button";
import Wrapper from "../../layouts/Wrapper";
import AdOrdersMenu from "./AdOrdersMenu";
import PanelPrevNext from "../general/PanelPrevNext";
import styles from "../../../scss/components/adOrders/AdOrdersHeader.module.scss";

const AdOrdersHeader = ({
  t,
  createNew,
  handleMenuActivity,
  menuActive,
  previousOrders,
  count,
  totalCount,
  nextOrders,
}) => (
  <Wrapper styleWrapper="wrapperColumn">
    <div className={styles.btnBox}>
      <Button color="buttonCreateNew" onClick={createNew}>
        {t("createNew")}
      </Button>
    </div>
    <AdOrdersMenu handleMenuActivity={handleMenuActivity} menuActive={menuActive} />
    <PanelPrevNext
      btnPrev={t("previous")}
      prev={t("prev")}
      previousOrders={previousOrders}
      count={count}
      totalCount={totalCount}
      ofText={t("of")}
      nextOrders={nextOrders}
      nextPrev={t("next")}
    />
  </Wrapper>
);

AdOrdersHeader.defaultProps = {
  createNew: () => {},
  handleMenuActivity: () => {},
  previousOrders: () => {},
  nextOrders: () => {},
  menuActive: {},
  count: {},
  totalCount: 0,
};

AdOrdersHeader.propTypes = {
  t: PropTypes.func.isRequired,
  createNew: PropTypes.func,
  handleMenuActivity: PropTypes.func,
  previousOrders: PropTypes.func,
  nextOrders: PropTypes.func,
  menuActive: PropTypes.shape({}),
  count: PropTypes.shape({}),
  totalCount: PropTypes.number,
};

export default withTranslation("adOrderPage")(AdOrdersHeader);
