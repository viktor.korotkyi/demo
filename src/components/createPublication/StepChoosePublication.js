import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Wrapper from "../../layouts/Wrapper";
import Button from "../UI/Button";

import styles from "../../../scss/components/createPublication/StepChoosePublication.module.scss";

const StepChoosePublication = ({ t, next, step, stepName, disabled }) => (
  <div className={styles.container}>
    <Wrapper styleWrapper="wrapperCreatePublicationStep">
      <span className={styles.header}>{`${t("step")} ${step} ${t("of")} 5 - ${stepName}`}</span>
      <div className={styles.lineBox}>
        <div className={styles.step} style={{ width: `${20 * step}%` }} />
        <div className={styles.seekbar} style={{ width: `${100 - 20 * step}%` }} />
      </div>
      <Button color="buttonSaveContinue" onClick={next} disabled={disabled}>
        {t("next")}
      </Button>
    </Wrapper>
  </div>
);

StepChoosePublication.defaultProps = {
  next: () => {},
  step: 1,
  stepName: "",
  disabled: false,
};

StepChoosePublication.propTypes = {
  t: PropTypes.func.isRequired,
  next: PropTypes.func,
  step: PropTypes.number,
  stepName: PropTypes.string,
  disabled: PropTypes.bool,
};

export default withTranslation("createPubPage")(StepChoosePublication);
