import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import ContentHeader from "./ContentHeader";
import BarChooseButton from "../UI/BarChooseButton";
import Wrapper from "../../layouts/Wrapper";
import styles from "../../../scss/components/createPublication/ChooseClassified.module.scss";

const ChooseClassified = ({
  t,
  listView,
  handleViewList,
  chooseClasseField,
  clickChooseClassifield,
  listChooseClassifield,
}) => (
  <div className={styles.container}>
    <Wrapper styleWrapper="wrapperCreatePublication" styleContainer="containerHightAuto">
      <ContentHeader
        handleViewList={handleViewList}
        headerText={t("chooseClassified")}
        listView={listView}
        text={t("underHeader")}
      />
      <div className={`${styles.contentList} ${!listView && styles.gallery}`}>
        {Object.keys(listChooseClassifield).map(
          (item, index) =>
            item !== "__typename" && (
              <BarChooseButton
                key={[index, item].join("_")}
                title={t(`${item}`)}
                idBlock={item}
                listButton={listChooseClassifield[item]}
                active={chooseClasseField[item]}
                clickChooseClassifield={clickChooseClassifield}
                listView={listView}
              />
            )
        )}
      </div>
    </Wrapper>
  </div>
);

ChooseClassified.defaultProps = {
  listView: true,
  handleViewList: () => {},
  clickChooseClassifield: () => {},
  chooseClasseField: {},
  listChooseClassifield: {},
};

ChooseClassified.propTypes = {
  t: PropTypes.func.isRequired,
  listView: PropTypes.bool,
  handleViewList: PropTypes.func,
  clickChooseClassifield: PropTypes.func,
  chooseClasseField: PropTypes.shape({}),
  listChooseClassifield: PropTypes.shape({}),
};

export default withTranslation("createPubPage")(ChooseClassified);
