import PropTypes from "prop-types";
import { faListUl, faThLarge } from "@fortawesome/free-solid-svg-icons";
import IconView from "../UI/IconView";
import styles from "../../../scss/components/createPublication/ContentHeader.module.scss";
import HeaderBox from "../general/HeaderBox";

const ContentHeader = ({ listView, handleViewList, headerText, text, watchBtn }) => (
  <div className={styles.container}>
    <HeaderBox header={headerText} text={text} />
    {watchBtn && (
      <div className={styles.btnPanel}>
        <IconView handleViewList={handleViewList} icon={faListUl} id="list" listView={listView} />
        <IconView
          handleViewList={handleViewList}
          icon={faThLarge}
          id="gallery"
          listView={!listView}
        />
      </div>
    )}
  </div>
);

ContentHeader.defaultProps = {
  listView: true,
  watchBtn: true,
  handleViewList: () => {},
  headerText: "",
  text: "",
};

ContentHeader.propTypes = {
  listView: PropTypes.bool,
  watchBtn: PropTypes.bool,
  handleViewList: PropTypes.func,
  headerText: PropTypes.string,
  text: PropTypes.string,
};

export default ContentHeader;
