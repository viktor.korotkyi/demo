import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Wrapper from "../../layouts/Wrapper";
import styles from "../../../scss/components/createPublication/ChoosePub.module.scss";
import PublicationList from "./PublicationList";

const ChoosePub = ({
  t,
  listView,
  handleViewList,
  groupOneNewsPapers,
  groupTwoNewsPapers,
  handleGroupOne,
  handleGroupTwo,
  groupOne,
  groupTwo,
  viewGroup,
}) => (
  <div className={styles.container}>
    <Wrapper styleWrapper="wrapperCreatePublication" styleContainer="containerHightAuto">
      <div className={styles.contentBox}>
        <PublicationList
          handleViewList={handleViewList}
          headerText={t("group1")}
          listView={listView}
          text={t("groupOneText")}
          list={groupOneNewsPapers}
          handleGroup={handleGroupOne}
          group={groupOne}
          hideBtn
        />
        {viewGroup && (
          <PublicationList
            handleViewList={handleViewList}
            headerText={t("group2")}
            listView={listView}
            text={t("groupSecondText")}
            watchBtn={false}
            list={groupTwoNewsPapers}
            handleGroup={handleGroupTwo}
            group={groupTwo}
            hideBtn
          />
        )}
      </div>
    </Wrapper>
  </div>
);

ChoosePub.defaultProps = {
  listView: true,
  viewGroup: false,
  handleViewList: () => {},
  handleGroupOne: () => {},
  handleGroupTwo: () => {},
  groupOne: {},
  groupTwo: {},
  groupOneNewsPapers: [],
  groupTwoNewsPapers: [],
};

ChoosePub.propTypes = {
  t: PropTypes.func.isRequired,
  listView: PropTypes.bool,
  viewGroup: PropTypes.bool,
  handleViewList: PropTypes.func,
  handleGroupOne: PropTypes.func,
  handleGroupTwo: PropTypes.func,
  groupOne: PropTypes.shape({}),
  groupTwo: PropTypes.shape({}),
  groupOneNewsPapers: PropTypes.arrayOf(PropTypes.shape({})),
  groupTwoNewsPapers: PropTypes.arrayOf(PropTypes.shape({})),
};

export default withTranslation("createPubPage")(ChoosePub);
