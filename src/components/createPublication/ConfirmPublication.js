import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import ContentHeader from "./ContentHeader";
import Wrapper from "../../layouts/Wrapper";
import ListGroupNewspapers from "../general/ListGroupNewspapers";

import styles from "../../../scss/components/createPublication/ConfirmPublication.module.scss";

const ConfirmPublication = ({
  t,
  handleViewList,
  listView,
  groupOneNewsPapers,
  handleGroupOne,
  groupOne,
  groupTwoNewsPapers,
  handleGroupTwo,
  groupTwo,
  viewGroup,
}) => (
  <div className={styles.container}>
    <Wrapper styleWrapper="wrapperCreatePublication" styleContainer="containerHightAuto">
      <ContentHeader
        handleViewList={handleViewList}
        headerText={t("confirmPub")}
        listView={listView}
        text={t("confirmText")}
      />

      <div className={styles.contentList}>
        <ListGroupNewspapers
          list={groupOneNewsPapers}
          listView={listView}
          handleGroup={handleGroupOne}
          group={groupOne}
          hideBtn
        />
      </div>
      {viewGroup && (
        <div className={styles.contentList}>
          <ListGroupNewspapers
            list={groupTwoNewsPapers}
            listView={listView}
            handleGroup={handleGroupTwo}
            group={groupTwo}
            hideBtn
          />
        </div>
      )}
    </Wrapper>
  </div>
);

ConfirmPublication.defaultProps = {
  listView: true,
  viewGroup: false,
  handleViewList: () => {},
  handleGroupOne: () => {},
  handleGroupTwo: () => {},
  groupOne: {},
  groupTwo: {},
  groupOneNewsPapers: [],
  groupTwoNewsPapers: [],
};

ConfirmPublication.propTypes = {
  t: PropTypes.func.isRequired,
  listView: PropTypes.bool,
  viewGroup: PropTypes.bool,
  handleViewList: PropTypes.func,
  handleGroupOne: PropTypes.func,
  handleGroupTwo: PropTypes.func,
  groupOne: PropTypes.shape({}),
  groupTwo: PropTypes.shape({}),
  groupOneNewsPapers: PropTypes.arrayOf(PropTypes.shape({})),
  groupTwoNewsPapers: PropTypes.arrayOf(PropTypes.shape({})),
};

export default withTranslation("createPubPage")(ConfirmPublication);
