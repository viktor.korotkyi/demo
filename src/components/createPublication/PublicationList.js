import PropTypes from "prop-types";
import ContentHeader from "./ContentHeader";
import ListGroupNewspapers from "../general/ListGroupNewspapers";
import styles from "../../../scss/components/createPublication/PublicationList.module.scss";

const PublicationList = ({
  handleViewList,
  headerText,
  listView,
  text,
  watchBtn,
  list,
  handleGroup,
  group,
  hideBtn,
}) => (
  <div className={styles.container}>
    <ContentHeader
      handleViewList={handleViewList}
      headerText={headerText}
      listView={listView}
      text={text}
      watchBtn={watchBtn}
    />
    <div className={styles.contentList}>
      <ListGroupNewspapers
        list={list}
        listView={listView}
        handleGroup={handleGroup}
        group={group}
        hideBtn={hideBtn}
      />
    </div>
  </div>
);

PublicationList.defaultProps = {
  listView: true,
  watchBtn: true,
  hideBtn: false,
  handleViewList: () => {},
  handleGroup: () => {},
  group: {},
  headerText: "",
  text: "",
  list: [],
};

PublicationList.propTypes = {
  listView: PropTypes.bool,
  watchBtn: PropTypes.bool,
  hideBtn: PropTypes.bool,
  handleViewList: PropTypes.func,
  handleGroup: PropTypes.func,
  group: PropTypes.shape({}),
  headerText: PropTypes.string,
  text: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({})),
};

export default PublicationList;
