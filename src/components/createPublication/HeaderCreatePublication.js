import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Button from "../UI/Button";
import Input from "../UI/Input";
import Arrow from "../UI/Arrow";
import Wrapper from "../../layouts/Wrapper";
import styles from "../../../scss/components/createPublication/HeaderCreatePublication.module.scss";

const HeaderCreatePublication = ({
  t,
  saveExit,
  edit,
  total,
  updateTotal,
  orderInfo,
  viewEdit,
  onChange,
}) => (
  <div className={styles.container}>
    <Wrapper styleWrapper="wrapperCreatePublicationHeader">
      <div className={styles.buttonBox}>
        <Button color="btnBlackTransparent" onClick={saveExit}>
          <Arrow />
          {t("saveExit")}
        </Button>
        <span className={styles.totalMobile}>{`${t("yourOrderTotal")} $${total}`}</span>
      </div>

      <div className={`${styles.order} ${viewEdit && styles.activeOrder}`}>
        <span>{t("yourAd")}</span>
        {!viewEdit &&
          (orderInfo ? (
            <span>{`${t("order")}${orderInfo}`}</span>
          ) : (
            <span className={styles.placeholderOrder}>{t("placeholderOrder")}</span>
          ))}

        {viewEdit && (
          <div className={styles.edit}>
            <span>{`${t("order")}`}</span>
            <Input
              autocomplete="off"
              maxlength="90"
              onChange={onChange}
              value={orderInfo}
              styleType="editOrder"
              autofocus
            />
          </div>
        )}
        <Button color="btnBlueTransparent" onClick={edit}>
          {!viewEdit ? t("edit") : t("apply")}
        </Button>
      </div>

      <div className={styles.editMobile}>
        <Button color="btnBlueTransparent" onClick={edit}>
          {!viewEdit ? t("edit") : t("apply")}
        </Button>
      </div>
      <span className={styles.total}>{`${t("yourOrderTotal")} $${total}`}</span>
      <span className={updateTotal ? styles.totalUpdate : styles.hide}>{t("orderUpdate")}</span>
    </Wrapper>
  </div>
);

HeaderCreatePublication.defaultProps = {
  saveExit: () => {},
  edit: () => {},
  onChange: () => {},
  total: "",
  updateTotal: false,
  viewEdit: false,
  orderInfo: "",
};

HeaderCreatePublication.propTypes = {
  t: PropTypes.func.isRequired,
  saveExit: PropTypes.func,
  edit: PropTypes.func,
  onChange: PropTypes.func,
  total: PropTypes.string,
  updateTotal: PropTypes.bool,
  viewEdit: PropTypes.bool,
  orderInfo: PropTypes.string,
};

export default withTranslation("createPubPage")(HeaderCreatePublication);
