import PropTypes from "prop-types";
import styles from "../../../scss/components/buildPublication/BuildPubView.module.scss";
import FormBuildPub from "../general/FormBuildPub";
import LivePreview from "../general/LivePreview";

const BuildPubView = ({
  headerDate,
  textFirst,
  textSecond,
  dateFirst,
  dateSecond,
  openModalDate,
  titleUpload,
  titleRightUpload,
  uploadFile,
  textPub,
  handleTextPub,
  titleLiveView,
  arrayInputs,
  chooseDate,
  deleteUploadFile,
  template,
  livePreview,
  handleCroppedImage,
  settingsCrop,
  onCropChange,
  onCropComplete,
  onZoomChange,
  onRotationChange,
  rotation,
  rotateleft,
  rotateRight,
  hideDateRange,
}) => (
  <div className={styles.container}>
    <div className={styles.forBox}>
      <FormBuildPub
        arrayInputs={arrayInputs}
        headerDate={headerDate}
        textFirst={textFirst}
        textSecond={textSecond}
        dateFirst={dateFirst}
        dateSecond={dateSecond}
        openModalDate={openModalDate}
        titleRightUpload={titleRightUpload}
        titleUpload={titleUpload}
        uploadFile={uploadFile}
        textPub={textPub}
        handleTextPub={handleTextPub}
        deleteUploadFile={deleteUploadFile}
        handleCroppedImage={handleCroppedImage}
        settingsCrop={settingsCrop}
        onCropChange={onCropChange}
        onCropComplete={onCropComplete}
        onZoomChange={onZoomChange}
        onRotationChange={onRotationChange}
        rotation={rotation}
        rotateleft={rotateleft}
        rotateRight={rotateRight}
        hideDateRange={hideDateRange}
      />
    </div>
    <div className={styles.viewBox}>
      <LivePreview
        titleLiveView={titleLiveView}
        textPub={textPub}
        chooseDate={chooseDate}
        template={template}
        livePreview={livePreview}
      />
    </div>
  </div>
);

BuildPubView.defaultProps = {
  headerDate: "",
  textFirst: "",
  textSecond: "",
  dateFirst: "",
  dateSecond: "",
  titleUpload: "",
  titleRightUpload: "",
  textPub: {},
  titleLiveView: "",
  handleTextPub: () => {},
  openModalDate: () => {},
  uploadFile: () => {},
  deleteUploadFile: () => {},
  arrayInputs: [],
  template: {},
  chooseDate: {},
  livePreview: false,
  handleCroppedImage: () => {},
  settingsCrop: {},
  onCropChange: () => {},
  onCropComplete: () => {},
  onZoomChange: () => {},
  onRotationChange: () => {},
  rotation: 0,
  rotateleft: () => {},
  rotateRight: () => {},
  hideDateRange: false,
};

BuildPubView.propTypes = {
  headerDate: PropTypes.string,
  textFirst: PropTypes.string,
  textSecond: PropTypes.string,
  dateFirst: PropTypes.string,
  dateSecond: PropTypes.string,
  titleUpload: PropTypes.string,
  titleRightUpload: PropTypes.string,
  titleLiveView: PropTypes.string,
  textPub: PropTypes.shape({}),
  handleTextPub: PropTypes.func,
  openModalDate: PropTypes.func,
  uploadFile: PropTypes.func,
  deleteUploadFile: PropTypes.func,
  arrayInputs: PropTypes.arrayOf(PropTypes.shape({})),
  chooseDate: PropTypes.shape({}),
  template: PropTypes.shape({}),
  livePreview: PropTypes.bool,
  handleCroppedImage: PropTypes.func,
  settingsCrop: PropTypes.shape({}),
  onCropChange: PropTypes.func,
  onCropComplete: PropTypes.func,
  onZoomChange: PropTypes.func,
  onRotationChange: PropTypes.func,
  rotation: PropTypes.number,
  rotateleft: PropTypes.func,
  rotateRight: PropTypes.func,
  hideDateRange: PropTypes.bool,
};

export default BuildPubView;
