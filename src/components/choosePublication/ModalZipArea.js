import PropTypes from "prop-types";
import styles from "../../../scss/components/choosePublication/ModalZipArea.module.scss";
import ModalLayout from "../../layouts/ModalLayout";
import Button from "../UI/Button";
import CloseButton from "../general/CloseButton";
import InputBox from "../UI/InputBox";
import CheckButton from "../UI/CheckButton";
import ButtonCount from "../UI/ButtonCount";

const ModalZipArea = ({
  headerModal,
  close,
  enterZipCode,
  statusModal,
  handleStatusModal,
  labelInput,
  handleInput,
  valueInput,
  continueBtn,
  continueModal,
  checkCity,
  handleCity,
}) => (
  <ModalLayout typeContainer="template">
    <div className={styles.container}>
      <div className={styles.closeHeader}>
        <h3>{headerModal}</h3>
        <CloseButton click={close} closeType="closeTemplate" />
      </div>
      <div className={styles.boxBtn}>
        <ButtonCount
          count="1"
          enterZipCode={enterZipCode}
          handleStatusModal={handleStatusModal}
          statusModal={statusModal}
          stylesType="typeModal"
          totalCount="2"
        />
        <ButtonCount
          count="2"
          enterZipCode={enterZipCode}
          handleStatusModal={handleStatusModal}
          statusModal={!statusModal}
          stylesType="typeModalLast"
          totalCount="2"
        />
      </div>
      {!statusModal ? (
        <div className={styles.inputBox}>
          <InputBox
            autocomplete="off"
            label={labelInput}
            onChange={handleInput}
            placeholder="55555"
            value={valueInput}
          />
        </div>
      ) : (
        <div className={styles.cityList}>
          {Object.keys(checkCity).map((item, index) => (
            <CheckButton
              key={[item, index].join("_")}
              active={checkCity[item]}
              id={item}
              onClick={handleCity}
              text={item}
            />
          ))}
        </div>
      )}

      <div className={styles.continue}>
        <Button color="payNowButton" onClick={continueModal} disabled={valueInput === ""}>
          {continueBtn}
        </Button>
      </div>
    </div>
  </ModalLayout>
);

ModalZipArea.defaultProps = {
  headerModal: "",
  close: () => {},
  enterZipCode: "",
  statusModal: false,
  handleStatusModal: () => {},
  labelInput: "",
  handleInput: () => {},
  valueInput: "",
  continueBtn: "",
  continueModal: () => {},
  checkCity: {},
  handleCity: () => {},
};

ModalZipArea.propTypes = {
  headerModal: PropTypes.string,
  close: PropTypes.func,
  enterZipCode: PropTypes.string,
  statusModal: PropTypes.bool,
  handleStatusModal: PropTypes.func,
  labelInput: PropTypes.string,
  handleInput: PropTypes.func,
  valueInput: PropTypes.string,
  continueBtn: PropTypes.string,
  continueModal: PropTypes.func,
  checkCity: PropTypes.shape({}),
  handleCity: PropTypes.func,
};

export default ModalZipArea;
