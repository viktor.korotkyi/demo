import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import InputBox from "../UI/InputBox";
import Button from "../UI/Button";
import LineAddress from "../general/LineAddress";
import LineCity from "../general/LinyCity";

import styles from "../../../scss/components/account/AccountForm.module.scss";

const AccountForm = ({ t, submitSave, form, handleForm, state, city }) => (
  <div className={styles.container}>
    <h3>{t("aboutYou")}</h3>
    <form className={styles.form} onSubmit={submitSave}>
      <InputBox
        id="email"
        label={t("emailAddress")}
        placeholder={t("emailAddress")}
        onChange={handleForm}
        name="email"
        value={form.email}
        type="email"
        textStyle="textNormal"
      />
      <InputBox
        id="phone"
        onChange={handleForm}
        label={t("phoneNumber")}
        placeholder={t("phoneNumber")}
        name="phone"
        value={form.phone}
        type="number"
        autocomplete="off"
        textStyle="textNormalMargin"
      />
      <LineAddress
        form={form}
        handleForm={handleForm}
        placeholderAddress={t("addressLine1")}
        labelFirst={t("addressLine1")}
        labelSecond={t("suite")}
      />
      <LineCity
        form={form}
        handleForm={handleForm}
        state={state}
        city={city}
        placeholderCity={t("city")}
        placeholderState={t("state")}
        chooseLabel={t("chooseOption")}
        labelFirst={t("city")}
        labelSecond={t("state")}
      />
      <InputBox
        id="number"
        onChange={handleForm}
        label={t("zip")}
        placeholder={t("zipCode")}
        name="zipCode"
        value={form.zipCode}
        type="number"
        autocomplete="off"
        textStyle="textNormal"
      />
      <div className={styles.btnBox}>
        <Button
          color="buttonSave"
          typeBtn={false}
          disabled={form.email === "" || form.phone === ""}
        >
          {t("save")}
        </Button>
      </div>
    </form>
  </div>
);

AccountForm.defaultProps = {
  submitSave: () => {},
  handleForm: () => {},
  form: {},
  formApply: {},
  state: [],
  city: [],
};

AccountForm.propTypes = {
  t: PropTypes.func.isRequired,
  submitSave: PropTypes.func,
  handleForm: PropTypes.func,
  form: PropTypes.shape({
    email: PropTypes.string,
    phone: PropTypes.string,
    zipCode: PropTypes.string,
  }),
  state: PropTypes.arrayOf(PropTypes.string),
  city: PropTypes.arrayOf(PropTypes.string),
  formApply: PropTypes.shape({}),
};

export default withTranslation("accountPage")(AccountForm);
