import PropTypes from "prop-types";
import { withTranslation } from "next-i18next";
import Button from "../UI/Button";
import styles from "../../../scss/components/account/AccountHeader.module.scss";
import Wrapper from "../../layouts/Wrapper";

const AccountHeader = ({ t, logout }) => (
  <Wrapper>
    <div className={styles.container}>
      <span className={styles.text}>{t("yourAccount")}</span>
      <Button onClick={logout} color="buttonTransparentColor">
        {t("logOut")}
      </Button>
    </div>
  </Wrapper>
);

AccountHeader.defaultProps = {
  logout: () => {},
};

AccountHeader.propTypes = {
  t: PropTypes.func.isRequired,
  logout: PropTypes.func,
};

export default withTranslation("accountPage")(AccountHeader);
